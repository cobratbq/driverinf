#ifndef _inf_startproc_H
#define _inf_startproc_H

#include <stdio.h>
#include "manufact.h"

int startProcedure(char *manulabel, char *modellabel, char *buffer, struct _ParsedArguments *args );

#endif