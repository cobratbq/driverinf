;
; VMUSB Setup Information file
; Copyright (c) VMware, Inc. 1998-2001
;

;******************************************************************************
; Version section
;------------------------------------------------------------------------------
[Version]
Signature="$CHICAGO$"
Provider=%S_Provider%
DriverVer=12/12/2000,1.41
Class=VMwareUSBDevices
ClassGUID={3B3E62A5-3556-4d7e-ADAD-F5FA3A712B56}


;******************************************************************************
; Class Install section (optional)
;------------------------------------------------------------------------------
;
; ### modify here ###
; The following sections are required if a private device class is used
; (see also the comments above).
; The sections should be removed completely if a system-defined class is used.
;
; install class, Windows 98:
[ClassInstall] 
AddReg=_AddReg_ClassInstall

; install class, Windows 2000:
[ClassInstall32] 
AddReg=_AddReg_ClassInstall

; registry entries required for class 
[_AddReg_ClassInstall]
HKR,,,,"%S_DeviceClassDisplayName%"
HKR,,Icon,,"-20"


;******************************************************************************
; Manufacturer section
;------------------------------------------------------------------------------
[Manufacturer]
%S_Mfg%=_Devices


;******************************************************************************
; Device section
;------------------------------------------------------------------------------
[_Devices]

%S_DeviceDesc1%=_Install1, USB\Vid_0e0f&Pid_0001

;******************************************************************************
; Install section ( Device1 )
;------------------------------------------------------------------------------
; driver-specific, Win98
[_Install1]
CopyFiles=_CopyFiles_sys
AddReg=_AddReg_SW1, _AddReg_98, _Parameters1_98

; device-specific, Win98
[_Install1.HW]
AddReg=_AddReg_HW1

; driver-specific, Win2000
[_Install1.NTx86]
CopyFiles=_CopyFiles_sys
AddReg=_AddReg_SW1, _Parameters1_NT

; service-install, Win2000
[_Install1.NTx86.Services]
AddService = %S_DriverName%, 0x00000002, _NT_AddService, _NT_EventLog

; device-specific, Win2000
[_Install1.NTx86.HW]
AddReg=_AddReg_HW1


;******************************************************************************
; Install section ( Device2 )
; may be repeated to support more than one device
;------------------------------------------------------------------------------
; ...


;******************************************************************************
; NT Service sections (Win2000 only)
;------------------------------------------------------------------------------
[_NT_AddService]
DisplayName    = %S_ServiceDisplayName%
ServiceType    = 1                  ; SERVICE_KERNEL_DRIVER
StartType      = 3                  ; demand
ErrorControl   = 1                  ; SERVICE_ERROR_NORMAL
ServiceBinary  = %10%\System32\Drivers\%S_DriverName%.sys

[_NT_EventLog]
AddReg=_NT_EventLog_AddReg

[_NT_EventLog_AddReg]
HKR,,EventMessageFile,0x00020000,"%%SystemRoot%%\System32\IoLogMsg.dll;%%SystemRoot%%\System32\drivers\%S_DriverName%.sys"
HKR,,TypesSupported,  0x00010001,7



;******************************************************************************
; Registry sections ( Device1 )
;------------------------------------------------------------------------------
[_AddReg_98]
; required standard entries
HKR,,NTMPDriver,,%S_DriverName%.sys
HKR,,DevLoader,,*ntkern

[_AddReg_SW1]
; create registry key used to store configuration parameters
HKLM,"%S_ConfigPath%",,,
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",,,

[_AddReg_HW1]
; Create a link to the configuration key. This is absolute required!
HKR,,USBIO_ConfigurationPath,,\Registry\Machine\%S_ConfigPath%\%S_DeviceConfigPath1%

;
; ### modify here ###
; Enable the next line to create an additional private user interface for your device.
; This is strongly recommended if you ship the driver together with a product.
; DO NOT USE an existing GUID. Instead, generate a new one !
; Use guidgen.exe to generate a new GUID, copy-paste it to the the following line.
HKR,,USBIO_UserInterfaceGuid,,"{2DA1FE75-AAB3-4d2c-ACDF-39088CADA665}"

;
; Enable the next line to create a static device name.
; THIS IS NOT RECOMMENDED !
; It should be used only if compatibility to earlier versions of VMUSB is required.
;HKR,,USBIO_DeviceBaseName,,%S_DeviceBaseName1%


; parameters used on Windows 98, Windows ME
[_Parameters1_98]
; ### modify here ###
; Set the default behaviour of the driver for Windows 98 and Windows ME.
; For a description of the parameters, refer to the VMUSB Reference Manual.
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",PowerStateOnOpen,0x00010001, 0
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",PowerStateOnClose,0x00010001, 0
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",EnableRemoteWakeup,0x00010001, 1
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",MinPowerStateUsed,0x00010001, 3
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",MinPowerStateUnused,0x00010001, 3
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",AbortPipesOnPowerDown,0x00010001, 1

; parameters used on Windows 2000
[_Parameters1_NT]
; ### modify here ###
; Set the default behaviour of the driver for Windows 2000
; For a description of the parameters, refer to the VMUSB Reference Manual.
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",PowerStateOnOpen,0x00010001, 0
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",PowerStateOnClose,0x00010001, 0
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",EnableRemoteWakeup,0x00010001, 1
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",MinPowerStateUsed,0x00010001, 3
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",MinPowerStateUnused,0x00010001, 3
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",AbortPipesOnPowerDown,0x00010001, 1
HKLM,"%S_ConfigPath%"\"%S_DeviceConfigPath1%",SuppressPnPRemoveDlg,0x00010001, 1


;******************************************************************************
; Registry sections ( Device2 )
; may be repeated to support more than one device
;------------------------------------------------------------------------------
; ...


;******************************************************************************
; Copy Files section
;------------------------------------------------------------------------------
[_CopyFiles_sys]
; ### modify here ###
; Specify the correct file name of the driver binary.
vmusb.sys


;******************************************************************************
; Options
;------------------------------------------------------------------------------
[ControlFlags]
; Advanced options can be specified here.
; For details, refer to the Windows 2000 DDK.


;******************************************************************************
; Destination Directories
;------------------------------------------------------------------------------
[DestinationDirs]
DefaultDestDir  = 11    ; SYSTEM directory
_CopyFiles_sys = 10,System32\Drivers


;******************************************************************************
; Disk Layout
;------------------------------------------------------------------------------
[SourceDisksNames]
1=%S_DiskName%,,

[SourceDisksFiles]
%S_DriverName%.sys=1


;******************************************************************************
; Text Strings
;------------------------------------------------------------------------------
[Strings]
; ### modify here ###
; Edit the strings in this section to met your preferences.
; Some of the strings are shown at the user interface.

; provider name
S_Provider="VMware, Inc."
; manufacturer name
S_Mfg="VMware, Inc."

; device class display name, shown in Device Manager
S_DeviceClassDisplayName="VMware USB devices"



; device description
S_DeviceDesc1="VMware USB Device"

; disk name
S_DiskName="VMware USB Driver Disk"


; file name of driver executable 
; If the name is modified, the _CopyFiles_sys section must also be modified !
S_DriverName="vmusb"

; service display name 
S_ServiceDisplayName="VMware USB Client Driver"


; configuration registry path (where the registry parameters will be located)
; IMPORTANT: MUST BE underneath SYSTEM\CurrentControlSet\Services !
; The string should be build in the form:
; SYSTEM\CurrentControlSet\Services\<DriverName>
S_ConfigPath="SYSTEM\CurrentControlSet\Services\vmusb"


; device-specific configuration registry path
S_DeviceConfigPath1="Parameters"

; device-specific base names
; Only needed if static device names are used, otherwise unused.
; See comments on the value USBIO_DeviceBaseName above.
S_DeviceBaseName1="USBIO_Device"


