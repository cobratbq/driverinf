; Copyright (c) 2002 Intel Corporation
; ****************************************************************************
; ****************************************************************************
; **    Filename:  IdeChnDr.INF
; **    Revision:  Version 2.2.2.2145
; **    Date:      08/05/2002
; **    Abstract:  Windows* INF File for Intel(R) Application Accelerator Driver
; ****************************************************************************
; ****************************************************************************

[version]
CatalogFile=IATA3000.cat
Signature="$WINDOWS NT$"
Class=hdc
ClassGuid={4D36E96A-E325-11CE-BFC1-08002BE10318}
Provider=%INTEL%
DriverVer=08/05/2002,2.2.2.2145

[DestinationDirs]
DefaultDestDir = 12 ; DIRID_DRIVERS
CoInstaller_CopyFiles = 11 ; System Dir( system32 on NT )
CopyBusDriver = 12;
CopyFullPort = 12;

[CopyBusDriver]
IdeBusDr.sys

[CopyFullPort]
IdeChnDr.sys

[CoInstaller_CopyFiles]
IPrtCnst.dll

[SourceDisksNames]
1 = %DiskName%,,,

[SourceDisksFiles]
IdeChnDr.sys= 1
IdeBusDr.sys= 1
IprtCnst.dll= 1

[ControlFlags]
ExcludeFromSelect=I_Primary_IDE_Channel
ExcludeFromSelect=I_Secondary_IDE_Channel
ExcludeFromSelect=PCI\VEN_8086&DEV_2411
ExcludeFromSelect=PCI\VEN_8086&DEV_2421
ExcludeFromSelect=PCI\VEN_8086&DEV_244B
ExcludeFromSelect=PCI\VEN_8086&DEV_248A
ExcludeFromSelect=PCI\VEN_8086&DEV_24CB

[Manufacturer]
%INTEL%=INTEL_HDC

[INTEL_HDC]
; DIDs for Channel Driver
%*PNP0600.PriDeviceDesc% = IdeChnDr_Inst_primary, I_Primary_IDE_Channel              ; Primary Channel
%*PNP0600.SecDeviceDesc% = IdeChnDr_Inst_secondary, I_Secondary_IDE_Channel          ; Secondary Channel

; DIDs for Bus Driver
%PCI\VEN_8086&DEV_2411.DeviceDesc%=IdeBusDr_Inst, PCI\VEN_8086&DEV_2411&CC_0101     ; Device ID for Intel ICH
%PCI\VEN_8086&DEV_2421.DeviceDesc%=IdeBusDr_Inst, PCI\VEN_8086&DEV_2421&CC_0101     ; Device ID for Intel ICH-0
%PCI\VEN_8086&DEV_244B.DeviceDesc%=IdeBusDr_Inst, PCI\VEN_8086&DEV_244B&CC_0101     ; Device ID for Intel ICH-2
%PCI\VEN_8086&DEV_248A.DeviceDesc%=IdeBusDr_Inst, PCI\VEN_8086&DEV_248A&CC_0101     ; Device ID for Intel ICH-3M
%PCI\VEN_8086&DEV_24CB.DeviceDesc%=IdeBusDr_Inst, PCI\VEN_8086&DEV_24CB&CC_0101     ; Device ID for Intel ICH-4H

;****************************************************
[IdeChnDr_Inst_primary]
AddReg         = IdeChnDr_Temp_parameters_AddReg

[IdeChnDr_Inst_secondary]
AddReg         = IdeChnDr_Temp_parameters_AddReg

[IdeChnDr_Temp_parameters_AddReg]
; Workaround for HT/CP hangs and command failures
HKLM,System\CurrentControlSet\Services\IdeChnDr\Parameters,"PrimaryCPI",%REG_DWORD%,0
HKLM,System\CurrentControlSet\Services\IdeChnDr\Parameters,"SecondaryCPI",%REG_DWORD%,0

[IdeChnDr_Inst_primary.Services]
AddService = IdeChnDr, %SPSVCINST_ASSOCSERVICE%, IdeChnDr_Service_Inst, IdeChnDr_EventLog_Inst

[IdeChnDr_Inst_secondary.Services]
AddService = IdeChnDr, %SPSVCINST_ASSOCSERVICE%, IdeChnDr_Service_Inst, IdeChnDr_EventLog_Inst

[IdeChnDr_Service_Inst]
DisplayName    = %*PNP0600.DeviceDesc%
ServiceType    = %SERVICE_KERNEL_DRIVER%
StartType      = %SERVICE_BOOT_START%
ErrorControl   = %SERVICE_ERROR_NORMAL%
ServiceBinary  = %12%\IdeChnDr.sys
LoadOrderGroup = SCSI Miniport
AddReg         = IdeChnDr_parameters_AddReg

[IdeChnDr_parameters_AddReg]
HKR,,Tag,%REG_DWORD%,25
HKR,Parameters,AutoConfiguration,%REG_DWORD%,0xffffffff
HKR,Parameters,UseLbaMode,%REG_DWORD%,0xffffffff
HKR,Parameters,UseMultiBlock,%REG_DWORD%,0xffffffff
HKR,Parameters,PrimaryMasterMode,%REG_DWORD%,0xffffffff
HKR,Parameters,PrimarySlaveMode,%REG_DWORD%,0xffffffff
HKR,Parameters,SecondaryMasterMode,%REG_DWORD%,0xffffffff
HKR,Parameters,SecondarySlaveMode,%REG_DWORD%,0xffffffff
HKR,Parameters,EnableFlush,%REG_DWORD%,0x0

[IdeChnDr_EventLog_Inst]
AddReg = IdeChnDr_EventLog_AddReg

[IdeChnDr_EventLog_AddReg]
HKR,,EventMessageFile,%REG_EXPAND_SZ%,"%SystemRoot%\System32\IoLogMsg.dll;%SystemRoot%\System32\drivers\IdeChnDr.sys"
HKR,,TypesSupported,%REG_DWORD%,7


;**********************************************************
; Intel PCI IDE Controller (ICH, ICH0, ICH2, and ICH3)
[IdeBusDr_Inst]
CopyFiles= CopyFullPort,CopyBusDriver

[IdeBusDr_Inst.Services]
AddService = IdeChnDr, 0, IdeChnDr_Service_Inst, IdeChnDr_EventLog_Inst
AddService = IdeBusDr, %SPSVCINST_ASSOCSERVICE%, IdeBusDr_Service_Inst, IdeChnDr_EventLog_Inst

[IdeBusDr_Service_Inst]
ServiceType    = %SERVICE_KERNEL_DRIVER%
StartType      = %SERVICE_BOOT_START%
ErrorControl   = %SERVICE_ERROR_NORMAL%
ServiceBinary  = %12%\IdeBusDr.sys
LoadOrderGroup = System Bus Extender

;-------------- Coinstaller installation
[IdeBusDr_Inst.CoInstallers]
AddReg=CoInstaller_AddReg
CopyFiles=CoInstaller_CopyFiles

[CoInstaller_AddReg]
;HKR,,CoInstallers32,0x00010000,"IPrtCnst.dll,IntelPortCoInstaller"
HKLM,System\CurrentControlSet\Control\CoDeviceInstallers,"{4D36E96A-E325-11CE-BFC1-08002BE10318}",0x00010008,"IPrtCnst.dll,IntelPortCoInstaller"

;********************************************************
; Extra Registry Entries

[Strings]
DiskName                = "Intel Application Accelerator Driver"
HDCClassName            = "IDE ATA/ATAPI controllers"
*PNP0600.DeviceDesc     = "Intel(R) Ultra ATA Controller"
*PNP0600.PriDeviceDesc  = "Primary IDE Channel"
*PNP0600.SecDeviceDesc  = "Secondary IDE Channel"
INTEL="Intel"
PCI\VEN_8086&DEV_2411.DeviceDesc="Intel(R) 82801AA Ultra ATA Controller"
PCI\VEN_8086&DEV_2421.DeviceDesc="Intel(R) 82801AB Ultra ATA Controller"
PCI\VEN_8086&DEV_244B.DeviceDesc="Intel(R) 82801BA Ultra ATA Controller"
PCI\VEN_8086&DEV_248A.DeviceDesc="Intel(R) 82801CAM Ultra ATA Controller"
PCI\VEN_8086&DEV_24CB.DeviceDesc="Intel(R) 82801DB Ultra ATA Controller"

;*******************************************
;Handy macro substitutions (non-localizable)
SPSVCINST_ASSOCSERVICE = 0x00000002
SERVICE_KERNEL_DRIVER  = 1
SERVICE_BOOT_START     = 0
SERVICE_ERROR_NORMAL   = 1
REG_EXPAND_SZ          = 0x00020000
REG_DWORD              = 0x00010001