#include <io.h>
#include "args.h"
#include "main.h"

int process_arguments( int iArgCount, char **args, struct _ParsedArguments *argsmem ) {
	int i, j;
	char *argument = NULL;

	if( iArgCount < 3 || strcmp(args[1],"/?") == 0 || strcmp(args[1],"-?") == 0 )
	{
		//Not enough arguments? let's them give some info
		if( argsmem->verbose > 0 ) {
			printf("\nUsage: %s <INF_FILE> <DEST_DIR> [WIN_DIR] [Extra Options]\n",args[0]);
			/*printf(" <INF_FILE>            : Path to INF File\n");	//path to the inf-file
			printf(" <DEST_DIR>            : Destination dir for driver\n");	//path to where driver file are to be copied to
			printf(" [WIN_DIR]             : Windows Directory (also retrieved from environment)\n");	//path to Windows directory
			printf("\nExtra Options:\n(OPTIONAL)\n");	//All extra options are optional, none are required.
			printf(" -NT                   : Force WinNT directory structure\n");	//Force the program to use NT directory structure
			printf(" -9x                   : Force Win9x directory structure\n");	//Force the program to use 9x directory structure
			printf(" -64                   : 64 Bit processor, look for special IA64 driver entries.");	//Search for 64bit architecture driver entries
			printf(" -L<labelname>         : Skip menu and jump to target-label\n");	//Search for given label instead of displaying a list of manufacturers and products.
			printf("\n");
			printf(" -PPn                  : Specify proc. n -> 0=Win40,1=Alpha,2=X86,3=IA64\n");
			printf(" -PVn                  : Specify drv. ver# n , ex. -PV3 (default)\n");
			printf("                       ( -PPn & -PVn are only used for printer driver backup )\n");
			printf("\n");
			printf(" -SN                   : Skip Needs directives\n");	//Skip all Needs entries that are encountered during backup.
			printf(" -SV                   : Skip Version check\n");	//Skip checking for version information, version info is currently not important
			printf(" -SF                   : Show Files, show path of every file\n");	//Show the full path of every file that is to be copied.
			printf(" -NCE                  : No Copy Errors, don't show copy errors\n");	//Do not show any copy errors.
			//printf(" -MEMDUMP              : Complete memory dump, show inf-file loaded in memory\n");	//Show the complete inf file, that is filtered and stored in the memory
			getch();
			printf(" -Vn                   : Verbose mode, n = level (0,1,2,3)\n");	//Tells the program how much info is to be displayed.
			printf(" -WAIT                 : Wait after completion of backup (used in front-end)\n");	//Wait after the program is done,
			printf(" -WOE                  : Wait on Error, wait only on error messages");*/
		}
													//gives you the oppertunity to read what is on the screen when U're using the front-end program.
		return 0; //We don't want to continue without arguments
	}
	else {
		for( i = 1; i < iArgCount; i++ ) {
			if( isargsign(args[i][0]) ) {
				
				//Copying argument
				argument = (char*)malloc((strlen(args[i])+1)*sizeof(char));
				for( j = 1; j < (int) strlen(args[i]); j++ )
					argument[j-1] = toupper(args[i][j]);
				argument[j] = '\0';

				//Setting Verbose mode based on argument
				if( argument[0] == 'V' ) {
					if( atoi((char*)&argument[1]) > 3 )
                        printf("\nVerbose mode was %d",argsmem->verbose);
					
					//Saving given verbose-level
					argsmem->verbose = atoi((char*)&argument[1]);

					if( argsmem->verbose > 0 )
						printf("\nSetting Verbose-mode to %d",argsmem->verbose);
				}
				else if( strcmp(argument,"NT") ) {
					argsmem->winver = VER_WINNT4;
					if( argsmem->verbose > 2 )
						printf("\nBesturingssysteem type: NT");
				}
				else if( strcmp(argument,"9X" ) ) {
					argsmem->winver = VER_WIN9598ME;
					if( argsmem->verbose > 2 )
						printf("\nBesturingssysteem type: 9x");
				}

				//free used memory for var argument
				free(argument);
			}
			else {
				//If not entered yet, copy Inf File
				if( argsmem->cInffile == NULL ) {
					argsmem->cInffile = (char*)malloc((strlen(args[i])+1)*sizeof(char));
					strcpy(argsmem->cInffile,args[i]);

					//Checking verbose mode, show info or not...
					if( argsmem->verbose > 2 )
						printf("\nTarget INF File: %s",argsmem->cInffile);
					
					//Check for existance of inffile
                    if( fileExists(argsmem->cInffile) ) {
						if( argsmem->verbose > 3 )
							printf("\nFound: %s",argsmem->cInffile);
					}
					else {
						printf("\nError: %s not found",argsmem->cInffile);
						return 0;
					}
				}
				//If not exist, copy Destination Dir
				else if( argsmem->cDestdir == NULL ) {
					argsmem->cDestdir = (char*)malloc((strlen(args[i])+1)*sizeof(char));
					strcpy(argsmem->cDestdir,args[i]);

					if( argsmem->verbose > 3 )
						printf("\nDestination Dir: %s",argsmem->cDestdir);
				}
				//If not exist, copy Windir from args.
				else if( argsmem->cWindir == NULL ) {
					argsmem->cWindir = (char*)malloc((strlen(args[i])+1)*sizeof(char));
					strcpy(argsmem->cWindir,args[i]);

					if( argsmem->verbose > 3 )
						printf("\nWindows dir: %s",argsmem->cWindir);
				}
				//If not one of three standard arguments, we have an illegal one :)
				else {
					//And we're gonna get rid of it!
					return 0;
				}
			}
		}
	}

	return 1;
}

void startup_config( struct _ParsedArguments *temp ) {
	
	temp->verbose = 3;
	temp->architecture = ARC_UNDEFINED;
	temp->winver = VER_UNDEFINED;
}

void shutdown_config( struct _ParsedArguments *temp ) {

	if( temp->cInffile != NULL )
		free(temp->cInffile);
	if( temp->cDestdir != NULL )
		free(temp->cDestdir);
	if( temp->cWindir != NULL )
		free(temp->cWindir);
}

int isargsign( char sign ) {
    if( sign == '-' )
		return 1;
	if( sign == '/' )
		return 1;

	return 0;
}

int fileExists( char *filename ) {
	if( _access(filename,0) )
		return 0;
	else return 1;
}