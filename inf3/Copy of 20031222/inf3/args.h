#ifndef _inf_args_H
#define _inf_args_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct _ParsedArguments {
	char *cInffile;		//INF file
	char *cDestdir;		//Dump directory
	char *cWindir;		//Windows directory
	
	int verbose;		//How much info do we get?
	int isWINNT;		//WinNT or Win9x
	int iLines;
	/*
	int showmem;		//Show memory dump and quit
	int is64bit;		//Is 64Bit Windows
	int no_copy_errors;//Don't display copy-errors
	int show_files;	//Show all files that are to be copied.
	int skip_needs;	//Don't parse the needs directive
	int skip_version_check;//Skip version check, don't drop on missing version label
	int wait_after_completion;	//pause screen after finished
	int wait_on_error;	//Only pause screen if finished and errors found.
	int error_encountered;//Have we had an error?
	int label_given;
	int linecount;
	int totalfiles;
	int copiedfiles;
	*/
};

int process_arguments( int iArgCount, char **args, struct _ParsedArguments *argsmem );
void startup_config( struct _ParsedArguments *temp );
void shutdown_config( struct _ParsedArguments *temp );
int fileExists( char *filename );
int isargsign( char sign );

#endif