#include "main.h"

struct _ParsedArguments arg;
struct _Inffile inf;
char cVerstring[] = "INF Backup Tool 3\n-----------------";

//Main Function: Start of the program
int main( int argc, char* argv[] ) {
	//Text on top of program
	//char *cManufacturer,*cTargetlabel;

	//Display program information
	printf(cVerstring);

	//Setting startup variables
	startup_config(&arg);

	//Parse program arguments
	if( !process_arguments(argc,argv,&arg) ) {
		if( arg.verbose > 0 )
            printf("\nArguments are not correct, please refer to commandline help: inf3.exe /?");

		shutdown_config(&arg);
		return -1;
	}

	inf.filename = arg.cInffile;
	infLoadfile(&inf);

	shutdown_inf(&inf);
	shutdown_config(&arg);
	return 0;
}