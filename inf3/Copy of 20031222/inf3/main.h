#ifndef _inf_main_H
#define _inf_main_H

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#include "args.h"
#include "inf.h"

#if !defined(_WIN32)
	#define _access access
#endif

#define INF_NORMAL_BUFFER 512
#define INF_MAX_BUFFER 1024

#endif