#include "args.h"
#include "inf.h"
#include "main.h"

//Load the inffile into a dynamic memory buffer.
int infLoadfile( struct _Inffile *inf ) {
	extern struct _ParsedArguments arg;
	char *buffer;
	unsigned long i,j, size;
	
	//open inffile in 'read text' mode
	inf->file = fopen(inf->filename,"rt");
	if( inf->file == NULL )
		return 0;

	//retrieve filesize and allocate memory
	size = getFilesize(inf->file)+1;
	inf->buffer = (char*)malloc(size*sizeof(char));
	if( inf->buffer == NULL ) {
		fclose(inf->file);
        return 0;
	}
    else if( arg.verbose > 3 )
        printf("\nUsed filebuffer size: %d",size);
	
	//clean allocated memory of weird signs
	for( i = 0; i < size; i++ )
		inf->buffer[i] = '\0';

	i = j = 0;
	do {
		buffer = (char*)malloc(INF_NORMAL_BUFFER*sizeof(char));
		
		infGetline(inf->file,buffer,INF_NORMAL_BUFFER,INF_MAX_BUFFER);
		infCleanline(buffer);
		
		//Reset j(counter) to beginning of local buffer.
		j = 0;
		if( strlen(buffer) > 0 ) {
			while(buffer[j] != '\0')
                inf->buffer[i++] = buffer[j++];
			inf->buffer[i++] = '\0';
		}

		//Free local buffer
		free(buffer);
	}
	while( !feof(inf->file) );
	
	//make a complete memorydump after cleaning the buffer
	if( arg.verbose > 3 ) {
		printf("\nMemory dump:\n---------------------");
		i = 0;
		while( strlen((char*)&inf->buffer[i]) != 0 ) {
			printf("\n%s",(char*)&inf->buffer[i]);
			i += (long)strlen((char*)&inf->buffer[i])+1;
		}
	}

	//close file
	fclose(inf->file);
	
	return 1;
}

//Read one line from inffile
//Need protection from 1024 buffer overflow
int infGetline( FILE *file, char *buffer, int normal, int max ) {
	char current;
	int i = 0;
	int returncode = 1;
	
	do {
		if( i == normal )
			buffer = realloc(buffer,max);

		current = fgetc(file);
		if( current == EOF )
			break;
		else if( current == '\n' )
			break;
		else buffer[i++] = current;
	}
	while(1);
	buffer[i] = '\0';
	
	return returncode;
}

//Clean read bufferline from comments and spaces.
int infCleanline( char *buffer ) {
   
	infRemovecomment(buffer);
	infRemovespaces(buffer);

	return 1;
}

//Remove comments from buffer
//Check for ;CopyFiles directives
void infRemovecomment( char *buffer ) {
	unsigned int i = 0;
	
	for( i = 0; i < strlen(buffer); i++ ) {
		if( buffer[i] == ';' ) {
			buffer[i] = '\0';
			break;
		}
	}
}

//Removes spaces and tabs from inffile-buffer if not surrounded by quotation-signs
void infRemovespaces( char* buffer ) {
	unsigned int i = 0;
	unsigned int j = 0;
	int quote = 0;
	
	char *temp;
	temp = (char*)malloc((strlen(buffer)+1)*sizeof(char));

	for( i = 0; i < strlen(buffer); i++ ) {
		if( ( buffer[i] == ' ' || buffer[i] == '\t' ) && quote == 0 )
			i = i; //dummy-statement just to do something ...
		else if( ( buffer[i] == ' ' || buffer[i] == '\t' ) && quote == 1 )
			temp[j++] = buffer[i];
		else if( buffer[i] == '\"' ) {
			if( quote == 0 )
				quote = 1;
			else quote = 0;

			temp[j++] = buffer[i];
		}
		else temp[j++] = buffer[i];
	}
	
	//If quotation not closed, this closes it!
	/*if( quote == 1 && strlen(buffer) < 511 )
		temp[j++] = '\"';*/

	temp[j] = '\0';

	strcpy(buffer,temp);

	free(temp);
}

//Retrieving filesize of opened file
long getFilesize( FILE *file ) {
	long length, curpos;

	curpos = ftell(file);
    fseek(file,0L,SEEK_END);
	length = ftell(file);
	fseek(file,curpos,SEEK_SET);
    
	return length;
}

//Close all open dynamic memory pointers of the struct.
void shutdown_inf( struct _Inffile *temp ) {

	if( temp->buffer != NULL )
		free(temp->buffer);

	if( temp->file != NULL )
		fclose(temp->file);
}