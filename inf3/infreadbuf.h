#ifndef _inf_infreadbuf_H
#define _inf_infreadbuf_H

#include <string.h>
#include <stdlib.h>

int irbFindlabel( char *labelname, char *buffer );
int irbFindstringvar( char *stringvarname, char *buffer, char *target );
int irbCorrectvar( char *line, char *buffer, char *target );
int splitString( char *line, char sign );
void strtoupper( char *string );

#endif