#include "version.h"

int verRetrieveinfo( struct _VersionInformation *vinfo, char *buffer) {
	int location;

	//Find Version label
	location = irbFindlabel("Version",buffer);

	if( location == -1 )
		return 0;

	printf("\nLocation: %d",location);
	printf("\n%s",&buffer[location]);

	//Parse labellocation for version information
	verParseversionlabel(vinfo,buffer,location);

	return 1;
}

//Do a replace of stringvar to real text!!!!
//Especially with Provider!
//Check for '=' inside quote!
void verParseversionlabel(struct _VersionInformation *info, char *buffer, int location ) {
	int loc = location;
	char *line;
	int pos, newsize;

	//While no other label appears.
	while( buffer[loc] != '[' && strlen(&buffer[loc]) > 0 ) {
		line = (char*)malloc((strlen(&buffer[loc])+1)*sizeof(char));
		strcpy(line,&buffer[loc]);

		if( (pos = splitString(line,'=')) != -1 ) {
			strtoupper(line);
			if( strcmp(line,"SIGNATURE") == 0 )
				strcpy(info->signature,&line[pos]);
			else if( strcmp(line,"CLASS") == 0 )
				strcpy(info->classname,&line[pos]);
			else if( strcmp(line,"CLASSGUID") == 0 )
				strcpy(info->classguid,&line[pos]);
			else if( strcmp(line,"PROVIDER") == 0 ) {

				//Remove all stringvars from provider-text
				newsize = irbCorrectvar(&line[pos],buffer,NULL);
				line = realloc(line,((pos+1+newsize+1)*sizeof(char)));
				line[pos+newsize+1] = '\0';
				irbCorrectvar(&line[pos],buffer,&line[pos]);
				
				//Allocate space in struct for provider string
				info->provider = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->provider,&line[pos]);
			}
			else if( strcmp(line,"LAYOUTFILE") == 0 ) {
				info->layout = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->layout,&line[pos]);
			}
			else if( strcmp(line,"CATALOGFILE") == 0 ) {
				info->catalog = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->catalog,&line[pos]);
			}
			else if( strcmp(line,"CATALOGFILE.NT") == 0 ) {
				info->catalog_nt = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->catalog_nt,&line[pos]);
			}
			else if( strcmp(line,"CATALOGFILE.NTX86") == 0 ) {
				info->catalog_ntx86 = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->catalog_ntx86,&line[pos]);
			}
			else if( strcmp(line,"CATALOGFILE.NTIA64") == 0 ) {
				info->catalog_ntia64 = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->catalog_ntia64,&line[pos]);
			}
			else if( strcmp(line,"DRIVERVER") == 0 ) {
				info->driverver = (char*)malloc((strlen(&line[pos])+1)*sizeof(char));
				strcpy(info->driverver,&line[pos]);
			}
		}

		loc += (int)strlen(&buffer[loc])+1;
		free(line);
	}

	printf("\nSignature: %s",info->signature);
	printf("\nClass: %s",info->classname);
	printf("\nClassGuid: %s",info->classguid);
	printf("\nProvider: %s",info->provider);
	printf("\nLayout: %s",info->layout);
	printf("\nCatalog: %s",info->catalog);
	printf("\nCatalog.nt: %s",info->catalog_nt);
	printf("\nCatalog.ntx86: %s",info->catalog_ntx86);
	printf("\nCatalog.ntia64: %s",info->catalog_ntia64);
	printf("\nDriverVer: %s",info->driverver);
}

void shutdown_version( struct _VersionInformation *temp ) {
	if( temp->provider != NULL )
		free(temp->provider);
	if( temp->layout != NULL )
		free(temp->layout);
	if( temp->catalog != NULL )
		free(temp->catalog);
	if( temp->catalog_nt != NULL )
		free(temp->catalog_nt);
	if( temp->catalog_ntx86 != NULL )
		free(temp->catalog_ntx86);
	if( temp->catalog_ntia64 != NULL )
		free(temp->catalog_ntia64);
	if( temp->driverver != NULL )
		free(temp->driverver);
}