#include "main.h"

struct _ParsedArguments arg;
struct _Inffile inf;
struct _VersionInformation ver;
char cVerstring[] = "INF Backup Tool 3\n-----------------";

//Main Function: Start of the program
int main( int argc, char* argv[] ) {

	//Display program information
	printf(cVerstring);

	//Setting startup variables
	startup_config(&arg);

	//Parse program arguments
	if( !process_arguments(argc,argv,&arg) ) {
		if( arg.verbose > 0 )
            printf("\nArguments are not correct, please refer to commandline help: inf3.exe /?");

		shutdown_config(&arg);
		return -1;
	}

	//Taking over pointer-address... this could give problems
	//when freeing the pointer(s) (with the same address)
	inf.filename = arg.cInffile;
	if( !infLoadfile(&inf) ) {
		if( arg.verbose > 0 )
			printf("\nError loading inffile!");

		shutdown_inf(&inf);
		shutdown_config(&arg);
		return -1;
	}

	//Read Version information from file and put it in the
	//inf struct. (This is mainly for informational purposes.
	if( !verRetrieveinfo(&ver,inf.buffer) ) {
		printf("\nCould not find all necessary versioninfo!");
		
		shutdown_inf(&inf);
		shutdown_config(&arg);
		return -1;
	}
	
	startProcedure(NULL,NULL,inf.buffer,&arg);
	
	//Close all open dyn. mem. pointers
	shutdown_version(&ver);
	shutdown_inf(&inf);
	shutdown_config(&arg);

	return 0;
}