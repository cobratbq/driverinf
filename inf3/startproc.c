#include "startproc.h"
#include "main.h"

int startProcedure(char *manulabel, char *modellabel, char *buffer, struct _ParsedArguments *args ) {
	char cManulabel[INF_NORMAL_BUFFER], cModellabel[INF_NORMAL_BUFFER];
	int choice;

	//If we have already given labels, we use them
	if( manulabel != NULL )
		strcpy(cManulabel,manulabel);
	else strcpy(cManulabel,"");

	if( modellabel != NULL )
		strcpy(cModellabel,modellabel);
	else strcpy(cModellabel,"");
	
	//If no label is given we ask for one
	if( strcmp(cManulabel,"") == 0 && strcmp(cModellabel,"") == 0 ) {
		listLabelcontents("manufacturer",buffer);
		printf("\nManufacturer: ");
		scanf("%d", &choice);
		
		chooseLabelcontents(choice,"manufacturer",buffer,cManulabel);
		
		//We pass on the cManulabel with all args attached, parseLabel filters out
		//the right argument and create the final labelname
		parseLabel(cManulabel,args, buffer);
		//realloc!!!

		printf("\n%s",cManulabel);
		getch();
	}

	//If no label is given we ask for one
	if( strcmp(cModellabel,"") == 0 ) {
		listLabelcontents(cManulabel,buffer);
		gets(cModellabel);
	}




	return 1;
}