#include "main.h"

int main( int argc, char *argv[] ) {
	int i, j;
	int size;
	char verstring[] = "\n--------------------------------------------------------------------------------Inf Backup Tool v0.90, written by Cobra, http://cobra.tmfweb.nl\nPlease send a bugreport with inf-file to: cobratbq@hotmail.com\n--------------------------------------------------------------------------------";
	char *tempstring;
	char *manufacturer, *model;
	int break_invalid_argument = 0;

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	//Gather all given arguments and use them in the appropriate way.
	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	if( argc < 3 || strcmp(argv[1],"/?") == 0 || strcmp(argv[1],"-?") == 0 )
	{
		//Not enough arguments? let's them give some info
		printf("%s",verstring);
		printf("Usage: %s <INF_FILE> <DEST_DIR> [WIN_DIR] [Extra Options]\n",argv[0]);
		printf(" <INF_FILE>            : Path to INF File\n");	//path to the inf-file
		printf(" <DEST_DIR>            : Destination dir for driver\n");	//path to where driver file are to be copied to
		printf(" [WIN_DIR]             : Windows Directory (also retrieved from environment)\n");	//path to Windows directory
		printf("\nExtra Options:\n(OPTIONAL)\n");	//All extra options are optional, none are required.
		printf(" -NT                   : Force WinNT directory structure\n");	//Force the program to use NT directory structure
		printf(" -9x                   : Force Win9x directory structure\n");	//Force the program to use 9x directory structure
		printf(" -64                   : 64 Bit processor, look for special IA64 driver entries.");	//Search for 64bit architecture driver entries
		printf(" -L<labelname>         : Skip menu and jump to target-label\n");	//Search for given label instead of displaying a list of manufacturers and products.
		printf("\n");
		printf(" -PPn                  : Specify proc. n -> 0=Win40,1=Alpha,2=X86,3=IA64\n");
		printf(" -PVn                  : Specify drv. ver# n , ex. -PV3 (default)\n");
		printf("                       ( -PPn & -PVn are only used for printer driver backup )\n");
		printf("\n");
		printf(" -SN                   : Skip Needs directives\n");	//Skip all Needs entries that are encountered during backup.
		printf(" -SV                   : Skip Version check\n");	//Skip checking for version information, version info is currently not important
		printf(" -SF                   : Show Files, show path of every file\n");	//Show the full path of every file that is to be copied.
		printf(" -NCE                  : No Copy Errors, don't show copy errors\n");	//Do not show any copy errors.
		//printf(" -MEMDUMP              : Complete memory dump, show inf-file loaded in memory\n");	//Show the complete inf file, that is filtered and stored in the memory
		getch();
        printf(" -Vn                   : Verbose mode, n = level (0,1,2,3)\n");	//Tells the program how much info is to be displayed.
		printf(" -WAIT                 : Wait after completion of backup (used in front-end)\n");	//Wait after the program is done,
		printf(" -WOE                  : Wait on Error, wait only on error messages");
													//gives you the oppertunity to read what is on the screen when U're using the front-end program.
		return -1; //We don't want to continue without arguments
	}
	else {
		for( i = 1; i < argc; i++ ) {
			if( argv[i][0] == '-' || argv[i][0] == '/' ) {
				tempstring = malloc((strlen(argv[i])+1) * sizeof(char));
				for( j = 1; j < (int) strlen(argv[i]); j++ )//We identified the first char as a '-' or '/' so we can continue with the rest of the argument
					tempstring[j-1] = toupper(argv[i][j]);	//to do this we remove the '-' or '/' by moving every char 1 place to the left.
				tempstring[j-1] = '\0';						//and after that we close the string.
				
				if( strcmp(tempstring,"NT") == 0 )
					isWINNT = 1;	//force NT directory structure by setting isWINNT to 1
				else if( strcmp(tempstring,"9X") == 0 )
					isWINNT = 0;	//force 9x directory structure by setting isWINNT to 0
				else if( strcmp(tempstring,"MEMDUMP") == 0 )
					showmem = 1;	//showmem is set to 1 to alert the program to dump the memory and exit.
				else if( strcmp(tempstring,"64") == 0 )
					is64BIT = 1;	//set to 1 if you have a 64bit architecture processor
				else if( strcmp(tempstring,"NCE") == 0 )
					no_copy_errors = 1;	//Copy errors will not be displayed.
				else if( strcmp(tempstring,"SF") == 0 )
					show_files = 1;	//Show path of every file to be copied.
				else if( strcmp(tempstring,"SN") == 0 )
					skip_needs = 1;	//Don't process needs-directives
				else if( strcmp(tempstring,"SV") == 0 )
					skip_version_check = 1;	//Do not drop if no version-info exist.
                else if( strcmp(tempstring,"WAIT") == 0 )
					wait_after_completion = 1;	//Wait after program completion.
				else if( strcmp(tempstring,"WOE") == 0 )
					wait_on_error = 1;
				else if( tempstring[0] == 'L' ) {
					for( j = 1; j < (int) strlen(argv[i]); j++ )
						tempstring[j-1] = tempstring[j];
					tempstring[j-1] = '\0';

					label_given = 1;	//skip to given label.

					model = malloc((strlen(tempstring)+1)*sizeof(char));
					strcpy(model,tempstring);

					manufacturer = malloc((strlen(tempstring)+1)*sizeof(char));
					strcpy(manufacturer,tempstring); //Dummy action to initialize dynamic mem manufacturer;
					//We do not need the manufacturer because the whole manufacturer and model parts are skipped,
					//because we already now which label to go to. But this initializes the memory so that we can
					//free it later on.
				}
				else if( tempstring[0] == 'V' ) {
					for( j = 1; j < (int) strlen(argv[i]); j++ )
						tempstring[j-1] = tempstring[j];
					tempstring[j-1] = '\0';

					if( strlen(tempstring) > 0 )
						verbose_mode = atoi(tempstring);	//This changes the amount of info to be displayed.

					if( verbose_mode == 0 )
						no_copy_errors = 1;
				}
				else if( tempstring[0] == 'P' && tempstring[1] == 'P' ) {
					for( j = 2; j < (int) strlen(argv[i]); j++ )
						tempstring[j-2] = tempstring[j];
					tempstring[j-2] = '\0';

					j = atoi(tempstring);
					if( j <= PR_MAX_PROC )
						pr_proc = j;
				}
				else if( tempstring[0] == 'P' && tempstring[1] == 'V' ) {
					for( j = 2; j < (int) strlen(argv[i]); j++ )
						tempstring[j-2] = tempstring[j];
					tempstring[j-2] = '\0';

					j = atoi(tempstring);
					pr_ver = j;
				}
				else {
					printf("\nInvalid argument used: %s",tempstring);	//If we find an invalid argument, we alert the user and exit
					error_encountered = 1;
					break_invalid_argument = 1;
				}
								
				free(tempstring);	//Freeing the tempstring memory that is temporarilly used to parse the arguments.
			}
			else if( cInfFile == NULL )
				cInfFile = argv[i];	//Set inf-file location
			else if( cDestDir == NULL )
				cDestDir = argv[i];	//Set destination directory
			else if( cWinDir == NULL )
				cWinDir = argv[i];	//set windows directory
		}
	}

	//if no specific processortype is given, we assume it's the default processor.
	if( pr_proc == PR_NODEF ) {
		if( is64BIT == 1 )
			pr_proc = PR_IA64;
		else pr_proc = PR_X86;
	}
	
	if( break_invalid_argument == 1 ) {	//If invalid argument is found, we exit the program.
		press_any_key();	//Press any key to continue ...
		return -1;	//exit program, because we found an invalid argument.
	}

    //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	//Check the whole environment for problems and break on any problem that we may encounter
	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

	printf("%s",verstring);
	if( check_environment() == -1 ) {	//Checking given arguments and other variables
		printf("\nAn error has occurred in program environment variables, aborting ...");
		error_encountered = 1;
		press_any_key();	//if an environment fail has been found, we exit the program.
		return -1;
	}

	//We load the inf file into the pc memory for filtering and quick accessing.
	if( (mInfFile = load_inf_into_memory(cInfFile,mInfFile)) == NULL ) { //Loading inf file into memory
		printf("\nAn error occurred while loading inf file into memory");
		error_encountered = 1;
		press_any_key();
		return -1;
	}

	//If user has given command to dump memory, we only show the memory
	if( showmem == 1 ) {	//after the memory dump we exit the program.
		free(mInfFile);
		press_any_key();
		return 0;
	}

	//Checking inf file for the necessary labels
	if( check_inf_file() == -1 ) {
		printf("\nCould not find all necessary labels");	//if we can not find a label that is necessary we exit the program.
		error_encountered = 1;
		free(mInfFile);
		press_any_key();
		return -1;
	}

	//Checking given cDestDir (DESTINATION DIRECTORY)
	if( cDestDir == NULL ) {
		printf("\nDestination Dir was not given");
		error_encountered = 1;
		return -1;
	}
	else {
		if( check_dir_existance(cDestDir,1) == -1 ) {
			printf("\nCould not create Destination Dir: %s",cDestDir);
			error_encountered = 1;
			return -1;
		}
	}
	if( showmem == 0 && verbose_mode > 0 ) {
		linecount++;
		printf("\nDestination dir: %s",cDestDir);
	}

	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	//Actual beginning of the program. (First question is asked, which manufacturer we want)
	//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	
	//If a label is given we skip the menu part.
	if( label_given == 0 ) {
		j = manufacturer_menu();					//Choose a manufacturer, store choice in 'j'
		if( j <= 0 ) {								//On exit we exit
			if( verbose_mode > 0 )
				printf("Exiting program ...");
			free(mInfFile);							//Cleaning memory
			press_any_key();
			return 0;								//Close program
		}

		//Retrieve size of name of label which is called on the given line-number
		size = get_label(j,NULL);	//retrieve size of mem for manufacturerlabel
		if( size == 0 ) {			//if size == 0 we exit program
			free(mInfFile);
			press_any_key();
			return -1;
		}
		else if( size == -1 ) {		//Sometimes there's only a string given, if so, the string is also the labelname
			size = get_stringislabel(j,NULL);	//therefore we use the function get_stringislabel()
			if( size == 0 ) {
				free(mInfFile);
				press_any_key();
				return -1;
			}
			manufacturer = malloc(size*sizeof(char));
			get_stringislabel(j,manufacturer);
		}
		else {
			manufacturer = malloc(size*sizeof(char));	//allocate mem for manufacturerlabel
			get_label(j,manufacturer); //get label on line j and write to manufacturer-pointer
		}
	
		if( verbose_mode > 0 ) {	//If verbose_mode > 0 we print info
			printf("\nChosen manufacturer: %s",manufacturer);
			linecount++;	//Lines count increases to be sure we stop at the right line.
		}

		j = models_menu(manufacturer);	//we give the label and we retrieve the number of the line with the chosen label
		if( j <= 0 ) {	//If choice == 0 we exit the program
			if( verbose_mode > 0 )
				printf("Exiting program ...");
			free(mInfFile);
			free(manufacturer);
			press_any_key();
			return -1;
		}
		size = get_label(j,NULL);	//Get the size of the chosen label
		if( size == 0 ) {
			free(mInfFile);
			press_any_key();
			return -1;
		}
		model = malloc(size*sizeof(char));	//allocate mem for label
		get_label(j,model);	//Retrieve label
	}
	
	//Initialize cCopyFiles & cNeeds & cCopyInfs
	//We do this so we can always add extra info to the variables, using realloc( ..., ...);
	cCopyFiles = malloc(1*sizeof(char));	//initialize cCopyFiles, cNeeds and cCopyInfs for later use
	cNeeds = malloc(1*sizeof(char));
	cCopyInfs = malloc(1*sizeof(char));
	strcpy(cCopyFiles,"");	//writing 'nothing' to the vars to make sure strcat function adds to the beginning of the var.
	strcpy(cNeeds,"");
	strcpy(cCopyInfs,"");

    tempstring = malloc((strlen(model)+strlen(".CoInstallers")+MAX_EXT_RESERVATION)*sizeof(char));	//With +20, with some extra space for extensions like .NTIA64 and .NTX86 and with the termination NULL
	//This section is WINNT only
	if( isWINNT == 1 ) {	//Is this OS based on New Technology (NT)
		//This section is WINNT_64BIT only
		if( is64BIT == 1 ) {	//Is this an IA64 based processor
			strcpy(tempstring,model);
			strcat(tempstring,".NTIA64");	//find label: modellabel.NTIA64
			if( find_label(tempstring) != 0 ) {
				cNeeds = realloc(cNeeds,(1+strlen(tempstring)+1)*sizeof(char));
				strcpy(cNeeds,":");
				strcat(cNeeds,tempstring);
			}
		}
		//This section is WINNT_32BIT only
		else if( is64BIT == 0 ) {
			strcpy(tempstring,model);
			strcat(tempstring,".NTX86");	//find label: modellabel.NTIA64
			if( find_label(tempstring) != 0 ) {
				cNeeds = realloc(cNeeds,(1+strlen(tempstring)+1)*sizeof(char));
				strcpy(cNeeds,":");
				strcat(cNeeds,tempstring);
			}
		}

		//This section is WINNT only
        if( strlen(cNeeds) == 0 ) {
			strcpy(tempstring,model);
			strcat(tempstring,".NT");	//find label: modellabel.NT
			if( find_label(tempstring) != 0 ) {
				cNeeds = realloc(cNeeds,(1+strlen(tempstring)+1)*sizeof(char));
				strcpy(cNeeds,":");
				strcat(cNeeds,tempstring);
			}
		}
	}
	//This section is for WIN9X & WINNT
	if( strlen(cNeeds) == 0 ) {
		strcpy(tempstring,model);	//find label: modellabel
		cNeeds = realloc(cNeeds,(1+strlen(tempstring)+1)*sizeof(char));
		strcpy(cNeeds,":");
		strcat(cNeeds,tempstring);
	}
	
	//Search for CoInstaller section
	strcat(tempstring,".CoInstallers");	//Search for CoInstaller: modellabel.CoInstallers
	if( find_label(tempstring) != 0 ) {
        cNeeds = realloc(cNeeds,(strlen(cNeeds)+1+1+strlen(tempstring)+1)*sizeof(char));
		strcat(cNeeds,",");
		strcat(cNeeds,":");
		strcat(cNeeds,tempstring);
	}
	free(tempstring);	//Free temporary string variable.

	if( verbose_mode > 0 ) printf("\nCopying files\n");
	copy_inffile();		//Copy inf file
    process_needs();	//Process labels in Needs list
	process_copyinfs();	//Copy inf files listed in CopyInfs list.
	process_copyfiles();//Copy files listed in CopyFiles list
	if( verbose_mode > 0 ) {
		printf("\n  %d out of %d files copied.",copiedfiles,totalfiles);	//Show amount of files listed and copied
		if( totalfiles != copiedfiles ) {
			printf("\n  %d files missing!",totalfiles-copiedfiles);			//Show amount of files missing
			error_encountered = 1;
		}
	}
	
	free(cNeeds);		//Free dynamic memory from pointers
	free(cCopyFiles);
	free(cCopyInfs);
	free(model);
	free(manufacturer);
	free(mInfFile);

	if( verbose_mode > 0 )
		printf("\n\nDone");	//Print 'Done' to console.

	press_any_key(); //Press any key to continue ...

	//printf("\nerror_encountered = %d\nWait on Error = %d",error_encountered,wait_on_error);
	return 0;	//Exit program.
}

void press_any_key( void ) { //And the famous 'Press any key to continue ... function!
	if( wait_on_error == 1 ) {
		if( error_encountered > 0 ) {
			if( verbose_mode > 0 )//Don't display text if we don't want to.
		        printf("\n\nPress any key to continue...");
			getch();
		}
	}
	else if( wait_after_completion == 1 ) {
		if( verbose_mode > 0 )//Don't display text if we don't want to.
			printf("\n\nPress any key to continue...");
		getch();
	}
}

int copy_inffile( void ) {
	char *temp;
	char *filename;
	int found, i;
    
    totalfiles++; //add 1 to total of files counted
	
	//Gather memory and combine strings and vars to get a dos command
	temp = malloc((strlen("copy \"")+strlen(cInfFile)+strlen("\" \"")+strlen(cDestDir)+strlen("\" >NUL")+1)*sizeof(char));
	strcpy(temp,"copy \"");
	strcat(temp,cInfFile);
	strcat(temp,"\" \"");
	strcat(temp,cDestDir);
	strcat(temp,"\" >NUL");	//we add the >NUL statement to make sure no irritating copy-info is displayed.
	
	//Check if we can find the file.
	if( _access(cInfFile,0) != 0 ) {	//Does the file actually exist? I hope so, don't you?
		printf("\nCould not copy inf file: %s",cInfFile);	//if it doesn't we report ofcourse!
		error_encountered = 1;
		return 0;
	}
	if( show_files == 1 ) printf("\n%s",cInfFile);
	system(temp);
	printf(".");
	free(temp);	//we free the command that we've just executed, we don't need it anymore.

	found = 0;
	for( i = (int)strlen(cInfFile)-1; i > 0; i-- ) {
		if( cInfFile[i] == '\\' ) {
			filename = &cInfFile[i+1];	//fishing for the filename
			found = 1;	//Yeah, we found the filename
			break;
		}
	}
	if( found == 0 )
		filename = &cInfFile[0];	//we record the position of the filename-beginning in a

	temp = malloc((strlen(cDestDir)+1+strlen(filename)+1)*sizeof(char));
    strcpy(temp,cDestDir);
	if( temp[strlen(temp)-1] != '\\' ) {	//making sure there's a '\' at the end of the path
		temp[strlen(temp)+1] = '\0';
		temp[strlen(temp)] = '\\';
	}
    strcat(temp,filename);	//adding filename to destination-path
	if( _access(temp,0) == 0 )	//checking if the file exists.
		copiedfiles++;	//We found the file, file's been copied ok so we add 1 to the copied files
	else {
		if( verbose_mode > 0 ) {
			printf("\nCould not find %s after copying!",temp);	//we report a missing file
			error_encountered = 1;
		}
	}
	free(temp);	//freeing temporary memory used for filepaths.

	return 0;
}

int process_copyinfs( void ) {
	unsigned int i, count, size;
	char dummy[1];
	char *temp, *destdir;
	
	i = 0;
	while( i < strlen(cCopyInfs) ) {	//Parse the cCopyInfs list.
		count = 0;

		while( cCopyInfs[count+i] != ',' && cCopyInfs[count+i] != '\0' )
			count++;	//Count the chars
		temp = malloc((count+1)*sizeof(char));
		count = 0;
		while( cCopyInfs[count+i] != ',' && cCopyInfs[count+i] != '\0' ) {
			temp[count] = cCopyInfs[count+i];	//write the chars to the temp
			count++;
		}
		temp[count] = '\0';	//close the 'temp' variable with a NULL character.

		dummy[0] = '\0';
		size = get_dirid(17,dummy,NULL);	//Get the INF directory, and add nothing (=dummy)
		destdir = malloc(size*sizeof(char));
		get_dirid(17,dummy,destdir);		//Retrieving DirID using a dummy to add nothing to the existing path.
		copy_file(temp,destdir,temp);		//copy the found file to the destination directory
		free(destdir);						//free destinationdir memory

		free(temp);							//free temp memory
		i += count + 1;						//adding to i to go to the next file in the list.
	}

	return 0;
}

int process_copyfiles( void ) {
	unsigned int i, count, size;
	char *temp;
	char *destdir;

	i = 0;
	while( i < strlen(cCopyFiles) ) { //While we stay inside the boundries of the var
		count = 0;	//Sub-counter 'count' resetted to 0

		while( cCopyFiles[count+i] != ',' && cCopyFiles[count+i] != '\0' )
			count++;	//Counting the chars before we hit a termination char.
		temp = malloc((count+1)*sizeof(char));	//Allocate space for text ('count' chars)
		count=0;		//Reset count
		while( cCopyFiles[count+i] != ',' && cCopyFiles[count+i] != '\0' ) {
			temp[count] = cCopyFiles[count+i];	//Until we hit the termination char, we copy chars to temp-var
			count++;
		}
		temp[count] = '\0';	//Close tempvar after loading.

		if( temp[0] != '@' ) {	//If a label is given (file is '@filename.ext')
			size = get_destdir(temp,NULL);
			if( size > 0 ) {	//if we can find a DestinationDir for this label
				destdir = malloc(size*sizeof(char));
				get_destdir(temp,destdir); //Retrieve DestinationDir using label.
				copy_files(temp,destdir); //Copy files in label to destinationdir.
				free(destdir);
			}
			else { //If we can't find a DestinationDir for this label
				size = get_destdir("DefaultDestDir",NULL);	//We retrieve the defaultdestinationdir.
				if( size > 0) {
					destdir = malloc(size*sizeof(char));
					get_destdir("DefaultDestDir",destdir);
					copy_files(temp,destdir); //Copy files in the given label to the given destinationdir.
					free(destdir);
				}
			}
		}
		else {	//If we copy only one file, we use the DefaultDestDir
			size = get_destdir("DefaultDestDir",NULL);
			if( size > 0 ) {
				destdir = malloc(size*sizeof(char));
				get_destdir("DefaultDestDir",destdir);
				copy_files(temp,destdir);
				free(destdir);
			}
		}
		free(temp);
		i += count + 1; //We add the temp-counter count to i, result: we start with the next word in the list.
	}
	return 0;
}

int get_destdir( char *labelname, char *destvar ) {	//labelname = label to search for, destvar = var to write dir to.
	char *newlabel;			//This function returns the destination directory, searching for using a labelname
	char *pointer;
	char *temp;
	char *desttemp;
	unsigned int i, count;
	unsigned int size = 0;
	unsigned long destnum = 0;

	newlabel = malloc((strlen(labelname)+1)*sizeof(char));	//We copy the given label to a new var, so we can alter it.
	strcpy(newlabel,labelname);
	for( i = 0; i < strlen(newlabel); i++ )
		newlabel[i] = toupper(newlabel[i]);	//changing to upper-case chars.

	pointer = &mInfFile[0];	//We start a pointer at the first line of the memory-duplicate inf
	for( i = 0; i < iDestDirs; i++ )	//we go to the destination dirs.
		pointer += strlen(pointer) + 1;

	while( i <= lines_InfFile ) {	//until we reach the end...
		if( pointer[0] == '[' )	//if we encounter the next label, we stop searching.
			break;

		if( pointer[0] == '\0' )	//if the first char is a NULL character, it is the end of this string.
			break;

		count = 0;
		while( pointer[count] != '\0' && pointer[count] != '=' )
			count++;	//count the chars
		temp = malloc((count+1)*sizeof(char));
		count = 0;
		while( pointer[count] != '\0' && pointer[count] != '=' ) {
			temp[count] = toupper(pointer[count]);	//write to temp
			count++;
		}
		temp[count] = '\0';	//close temp

		if( strcmp(newlabel,temp) == 0 ) {	//if label == given label
			free(temp);
			
			pointer += count + 1;	//Add strlen(label+terminationchar)

			count = 0;
			while( pointer[count] != ',' && pointer[count] != '\0' )
				count++;	//count chars
			temp = malloc((count+1)*sizeof(char));
			count = 0;
			while( pointer[count] != ',' && pointer[count] != '\0' ) {
				temp[count] = pointer[count];
				count++;	//write chars to temp
			}
			temp[count] = '\0';	//close temp
			
			destnum = atol(temp);	//convert char-number to long
			if( pointer[count] == ',' )
				pointer += count + 1;	//if we get additional path ...
			else if( pointer[count] == '\0' )
				pointer += count; //now the first char is a '\0', so we add nothing to the dirid string.
          
            count = get_dirid(destnum,pointer,NULL);
			if( count > 0 ) {
				desttemp = malloc(count*sizeof(char));
				get_dirid(destnum,pointer,desttemp);
				if( destvar != NULL )	//if a destiantion var is given, we write to that var
					strcpy(destvar,desttemp);
				size = (int) strlen(desttemp) + 1;	//returncode = strlen of directory
				free(desttemp);
				break;
			}
		}
		free(temp);

		pointer += strlen(pointer) + 1;
	}
	free(newlabel);

	return size; //Return size of directory (or return 0)
}

int copy_files( char *labelname, char *destdir ) {
	unsigned int label_line, i, count;
	char *pointer;
	char *targetfile;
	char *sourcefile;
	
	if( labelname[0] != '@' ) {
		if( (label_line = find_label(labelname)) == 0 && label_line > lines_InfFile )
			return -1;

		pointer = &mInfFile[0];
		for( i = 0; i < label_line; i++ )
			pointer += strlen(pointer) + 1;	//Search for the right line.
	
		for( i = label_line; i <= lines_InfFile; i++ ) {	//Process until we are at the end of our inf-file loaded in the memory
			if( pointer[0] == '[' )	//On new label, we quit
				break;
	
			count = 0;
			while( pointer[count] != ',' && pointer[count] != '\0' && pointer[count] != '=' )
				count++;
	
			targetfile = malloc((count+1)*sizeof(char));
			count = 0;
			while( pointer[count] != ',' && pointer[count] != '\0' && pointer[count] != '=' ) {
				targetfile[count] = pointer[count];
				count++;
			}
			targetfile[count] = '\0';
	
			if( pointer[count] == ',' ) {
				pointer += count + 1;
				
				count = 0;
				while( pointer[count] != ',' && pointer[count] != '\0' && pointer[count] != '=' )
					count++;
	
				sourcefile = malloc((count+1)*sizeof(char));
				count = 0;
				while( pointer[count] != ',' && pointer[count] != '\0' && pointer[count] != '=' ) {
					sourcefile[count] = pointer[count];
					count++;
				}
				sourcefile[count] = '\0';
			}
			else {
				sourcefile = malloc((strlen(targetfile)+1)*sizeof(char));
				strcpy(sourcefile,targetfile);
			}	
			copy_file(targetfile,destdir,sourcefile);

			free(sourcefile);
			free(targetfile);

			pointer += strlen(pointer) + 1;
		}
	}
	else {
		targetfile = malloc((strlen(labelname)+1)*sizeof(char));
		for( i = 1; i < strlen(labelname); i++ )
			targetfile[i-1] = labelname[i];
		targetfile[i-1] = '\0';
		copy_file(targetfile,destdir,targetfile);
	}

    return 0;
}

int copy_file(char *target, char *destdir, char *source ) { //target = filename of file when installed
	char *targetfile;										//source = filename of file before installation
	char *corrected_target;
	char *sourcefile;
	char *copy_command;
	int i, skip;

	skip = 0;
	corrected_target = malloc((strlen(target)+1)*sizeof(char));
	for( i = 0; i <= (int)strlen(target); i++ ) {
		if( target[i+skip] == '\"' )
			skip++;
        corrected_target[i] = target[i+skip];
	}
	//printf("\n%s",corrected_target);
	
	totalfiles++;

	targetfile = malloc((strlen(destdir)+1+strlen(corrected_target)+1) * sizeof(char)); //destdir+'\\'+filename+'\0'
	strcpy(targetfile,destdir);
	if( targetfile[strlen(targetfile)-1] != '\\' )
		strcat(targetfile,"\\");
	strcat(targetfile,corrected_target);
	
	if( strlen(source) > 0 ) {	//If source is empty, like with entry: filename.drv,, , we use targetfilename
		sourcefile = malloc((strlen(cDestDir)+1+strlen(source)+1)*sizeof(char));
		strcpy(sourcefile,cDestDir);
		if( sourcefile[strlen(sourcefile)-1] != '\\' )
			strcat(sourcefile,"\\"); //Fill sourcefile var with Destination dir.
	    strcat(sourcefile,source);	//If source is given, we copy source filename
	}
	else {
		sourcefile = malloc((strlen(cDestDir)+1+strlen(target)+1)*sizeof(char));
		strcpy(sourcefile,cDestDir);
		if( sourcefile[strlen(sourcefile)-1] != '\\' )
			strcat(sourcefile,"\\"); //Fill sourcefile var with Destination dir.
	    strcat(sourcefile,target);	//If source is not given, we copy to target filename
	}
	
	
	if( show_files == 1 ) {
		if( strcmp(target,source) == 0 || strlen(source) == 0 )
			printf("\n%s",targetfile);
		else printf("\n%s (as %s)",targetfile,source);
	}

	copy_command = malloc((strlen("copy \"")+strlen(targetfile)+strlen("\" \"")+strlen(sourcefile)+strlen("\" >NUL")+1) * sizeof(char));
	strcpy(copy_command,"copy \"");
	strcat(copy_command,targetfile);
	strcat(copy_command,"\" \"");
	strcat(copy_command,sourcefile);
	strcat(copy_command,"\" >NUL");
	
	if( _access(targetfile,0) == 0 ) {
		if( verbose_mode > 2 )
			printf("\n%s",copy_command);
		system(copy_command);
		printf(".");
		if( _access(targetfile,0) == 0 )
            copiedfiles++;
		else {
			printf("Could not find %s after copying!\n",sourcefile);
			error_encountered = 1;
		}
	}
	else if( no_copy_errors != 1 ) {
		printf("\nCould not find: %s",targetfile);
		error_encountered = 1;
	}

	free(copy_command);
	free(sourcefile);
	free(targetfile);
	free(corrected_target);

	return 0;
}

int process_needs( void ) {
	unsigned int i, count;
	char *needs_file, *needs_label;

	i = 0;
	while( i < strlen(cNeeds) ) {
		count = 0;
		
		while( cNeeds[count+i] != ':' )
			count++;
		needs_file = malloc((count+1)*sizeof(char));
		count = 0;
		while( cNeeds[count+i] != ':' ) {
			needs_file[count] = cNeeds[count+i];
			count++;
		}
		needs_file[count] = '\0';

		i += count + 1; // +1 = to skip :
		count = 0;
		while( cNeeds[count+i] != '\0' && cNeeds[count+i] != ',' )
			count++;
		needs_label = malloc((count+1)*sizeof(char));
		count = 0;
		while( cNeeds[count+i] != '\0' && cNeeds[count+i] != ',' ) {
			needs_label[count] = cNeeds[count+i];
			count++;
		}
		needs_label[count] = '\0';

		//The called function reads the label and edits global vars of cCopyFiles and cNeeds if directives are found.
		process_label(needs_label);

		free(needs_file);
		free(needs_label);
		
		i += count + 1;
	}

	return 0;
}

int process_label(char *labelname) {	//This function goes through a whole label, and catches the necessary directives.
	unsigned int i, count, label_line;
	unsigned int j, count2;
	int done;
	char *pointer;
	char *temp;

	char *list_needs, *list_includes;

	list_needs = malloc(1*sizeof(char));
	list_includes = malloc(1*sizeof(char));
	strcpy(list_needs,"");
	strcpy(list_includes,"");

	done = 0;
	if( (label_line=find_label(labelname)) == 0 || label_line > lines_InfFile )
		return -1;

	pointer = &mInfFile[0];
	for( i = 0; i < label_line; i++ )
		pointer += strlen(pointer) + 1;

	count = label_line;
	while( count <= lines_InfFile ) {
		i = 0;

		if( pointer[0] == '[' )
			break;

		while( pointer[i++] != '=' );
		temp = malloc((i+1)*sizeof(char));
		
		i = 0;
		while( pointer[i] != '=' ) {
			temp[i] = pointer[i];
			i++;
		}
		temp[i] = '\0';

		for( i = 0; i < strlen(temp); i++ )
			temp[i] = toupper(temp[i]);

		if( strcmp(temp,"COPYFILES") == 0 ) {
			i++; //(i == '=' (see above), so we skip 1 char)
			pointer += i;

            cCopyFiles = realloc(cCopyFiles,(strlen(cCopyFiles)+1+strlen(pointer)+1) * sizeof(char));
			if( strlen(cCopyFiles) != 0 )
				strcat(cCopyFiles,",");
			strcat(cCopyFiles,pointer);
		}
		else if( strcmp(temp,"INCLUDE") == 0 ) {
			i++;
			pointer += i;

			list_includes = realloc(list_includes,(strlen(list_includes)+strlen(pointer)+2)*sizeof(char));
			if( strlen(list_includes) != 0 )
				strcat(list_includes,",");
			strcat(list_includes,pointer);
		}
		else if( strcmp(temp,"NEEDS") == 0 ) {
			i++;
			pointer += i;

			list_needs = realloc(list_needs,(strlen(list_needs)+strlen(pointer)+2)*sizeof(char));
			if( strlen(list_needs) != 0 )
				strcat(list_needs,",");
			strcat(list_needs,pointer);
		}
		else if( strcmp(temp,"COPYINF") == 0 ) {
			i++;
			pointer += i;

			cCopyInfs = realloc(cCopyInfs,(strlen(cCopyInfs)+strlen(pointer)+2)*sizeof(char));
			if( strlen(cCopyInfs) != 0 )
				strcat(cCopyInfs,",");
			strcat(cCopyInfs,pointer);
		}

		free(temp);
		count++;
		pointer += strlen(pointer) + 1;
	}


	done = count = count2 = i = j = 0; //We process the needs and includes to find a needs directive without a filename,
										//All given files are system files, but only a labelname indicates another label within this inf file.
	while( done != 1 && skip_needs != 1 && (strlen(list_includes) != 0 || strlen(list_needs) != 0 ) ) {
		i = 0;
		while( list_includes[i+count] != ',' ) {
			if( list_includes[i+count] == '\0' )
				break;
			i++;
		}

		j = 0;
		while( list_needs[j+count2] != ',' ) {
			if( list_needs[j+count2] == '\0' )
				break;
			j++;
		}

		temp = malloc((i+2)*sizeof(char));
		i = 0;
		while( list_includes[i+count] != ',' ) {
			if( list_includes[i+count] == '\0' )
				break;
			temp[i] = list_includes[i+count];
			i++;
		}
		temp[i] = ':';
		temp[i+1] = '\0';

		temp = realloc(temp,(strlen(temp)+j+1)*sizeof(char));
		j = 0;
		while( list_needs[j+count2] != ',' ) {
			if( list_needs[j+count2] == '\0' )
				break;
			temp[i+1+j] = list_needs[j+count2];
			j++;
		}
		temp[i+1+j] = '\0';

		if( temp[0] == ':' ) {	//We only add a Needs directive if no file is given,
								//Microsoft's docs say that all given includes are system-supplied.
			cNeeds = realloc(cNeeds,(strlen(cNeeds)+1+strlen(temp)+1)*sizeof(char));
			strcat(cNeeds,",");
			strcat(cNeeds,temp);
		}
        
        free(temp);

		count += i + 1;
		if( count > strlen(list_includes) || count2 > strlen(list_needs) )
			break;
	}
	
	free(list_includes);
	free(list_needs);

	return 0;
}

int models_menu( char *label ) {
	unsigned int label_line;
	unsigned int i;
    int choice;
	int templabelsize;
	char *tempstring, *templabel;

	char *pointer;
	
	linecount = 0;

	label_line = find_label(label);
	if( label_line == 0 ) {
		printf("\nCould not find label \"%s\"",label);
		error_encountered = 1;
		return -1;
	}

	if( label_line > lines_InfFile )
		return -1;

	pointer = &mInfFile[0];
	for( i = 0; i < label_line; i++ )
		pointer += strlen(pointer) + 1;

	choice = 1;
	for( i = label_line; i <= lines_InfFile; i++ ) {
		if( pointer[0] == '[' )
			break;
		tempstring = malloc(varstotext(pointer,NULL) * sizeof(char));
		varstotext(pointer,tempstring);
		
		printf("\n%d: %s",choice,tempstring);

		if( verbose_mode > 1 ) {
            templabelsize = get_label(label_line+choice-1,NULL);
			templabel = malloc(templabelsize*sizeof(char));
			get_label(label_line+choice-1,templabel);
			printf(" (%s)",templabel);
			free(templabel);
		}

		choice++;
		pointer += strlen(pointer) + 1;

		free(tempstring);

		if( ++linecount >= 24 ) {
			linecount = 0;
			printf("\nPress any key to continue ...");
			getch();
		}
	}

	printf("\nChoose: ");
	scanf("%d",&choice);
	linecount += 3;

	if( choice == 0 )
		return 0;
	else return label_line+choice-1;
}

int manufacturer_menu( void ) {
	unsigned int i;
	char *pointer;
	char *tempstring;
	int choice;

    pointer = &mInfFile[0];
	for( i = 0; i < iManufacturer; i++ ) //Search for the right line
		pointer += strlen(pointer) + 1; //If not the right line, get next line
	
	choice = 1;
	for( i = iManufacturer; i <= lines_InfFile; i++ ) {
		if( pointer[0] == '[' )
			break;
		tempstring = malloc(varstotext(pointer,NULL) * sizeof(char));
		varstotext(pointer,tempstring);
		
		printf("\n%d: %s",choice++,tempstring);
		pointer += strlen(pointer) + 1;

		free(tempstring);

		if( ++linecount >= 24 ) {
			linecount = 0;
			printf("\nPress any key to continue ...");
			getch();
		}
	}

	printf("\nChoose: ");
	scanf("%d",&choice);

	if( choice == 0 )
		return 0;
	else return iManufacturer+choice-1;	//We need to have the manufacturers part, to get to the right beginning line.
									//we add the chosen number, and we decrease it with 1 because the computer count
									//beginning with zero!
}

int varstotext(char *text, char *destination) {
	int i;
	int count;
	int quote;
	char *tempstring;
	char *temp;
	char *temp2;

	tempstring = malloc(1);
	strcpy(tempstring,"");

	quote = 0;
	i = 0;

	for( i = 0; i < (int) strlen(text); i++ ) {
		if( text[i] == '=' )
			break;
        else if( text[i] == '\"' ) {
			if( quote == 0 )
				quote = 1;
			else quote = 0;
		}
		/*	i++; //The current char is the ", so we need to skip this one.
			count = i;
			while( text[count++] != '\"' );
			
			tempstring = realloc(tempstring,strlen(tempstring) + count + 1);
			count = (int) strlen(tempstring);

			while( text[i] != '\"' )
				tempstring[count++] = text[i++];
			tempstring[count] = '\0';
		}*/
		else if( text[i] == '%' ) {
			i++;
			count = i;

			while( text[count++] != '%' );

			temp = malloc((count+1) * sizeof(char));
			count = 0;
			while( text[i] != '%' )
				temp[count++] = text[i++];
			temp[count] = '\0';

			count = get_stringvar(temp,NULL);
            temp2 = malloc(count * sizeof(char));
			get_stringvar(temp,temp2);
			tempstring = realloc(tempstring,strlen(tempstring)+count+1);
			strcat(tempstring,temp2);
			free(temp2);
			free(temp);
		}
		else if( quote == 1 ) {
			tempstring = realloc(tempstring,strlen(tempstring)+2);
			temp2 = malloc(2*sizeof(char));
			temp2[0] = text[i];
			temp2[1] = '\0';
			strcat(tempstring,temp2);
			free(temp2);
		}
	}

	if( destination != NULL )
		strcpy(destination,tempstring);
	count = (int) strlen(tempstring) + 1;
	free(tempstring);
	return count;
}


/* Sometimes you find an INF File where there's only the first part of the manufacturer-menu, example:
[MANUFACTURER]
"HP"

In such a case, the name is also the label, so this manufacturer goes to label 'HP'.
*/
int get_stringislabel(int linenumber, char *destination) {
	int i;
	int returncode = 0;
	char *tempstring;
	char *pointer;

	if( linenumber > (int) lines_InfFile )
		return 0;

	pointer = &mInfFile[0];
	for( i = 0; i < (int) linenumber; i++ )
		pointer += strlen(pointer) + 1;

	i = 0;
	if( pointer[0] != '\"' )
		return 0;
	pointer += 1;

	tempstring = malloc((strlen(pointer)+1)*sizeof(char));
	strcpy(tempstring,pointer);
	for( i = 0; i < (int) strlen(tempstring); i++ )
		if( tempstring[i] == '\"' )
			tempstring[i] = '\0';

	returncode = (int) strlen(tempstring) + 1;
	if( destination != NULL )
		strcpy(destination,tempstring);
	free(tempstring);

	return returncode;
}

int get_label(int linenumber, char *destination) {
	int i;
	int returncode = 0;
	char *tempstring;
	char *pointer;

	if( linenumber > (int) lines_InfFile )
		return 0;

	pointer = &mInfFile[0];
	for( i = 0; i < (int) linenumber; i++ )
		pointer += strlen(pointer) + 1;

	i = 0;
	while( pointer[i] != '=' ) {
		if( pointer[i++] == '\0' )
			return -1;
	}

	pointer += i + 1;
	tempstring = malloc((strlen(pointer)+1)*sizeof(char));
	strcpy(tempstring,pointer);
	for( i = 0; i < (int) strlen(tempstring); i++ )
		if( tempstring[i] == ',' )
			tempstring[i] = '\0';
	
	returncode = (int) strlen(tempstring) + 1;
	if( destination != NULL )
		strcpy(destination,tempstring);
	free(tempstring);

	return returncode;
}

int get_stringvar( char *var, char *destination ) {
	unsigned int i, j;
	char *pointer;
	char *temp;
	char *ovar;
	int returncode = 0;

	ovar = malloc((strlen(var)+1) * sizeof(char));
	strcpy(ovar,var);
	for( i = 0; i < strlen(ovar); i++ )
		ovar[i] = toupper(ovar[i]);
	
	pointer = &mInfFile[0];
	for( i = 0; i < iStrings; i++ )
		pointer += strlen(pointer) + 1;
	
	for( i = iStrings; i <= lines_InfFile; i++ ) {
		j = 0;
		while( pointer[j++] != '=' );
		temp = malloc((j+1) * sizeof(char));

		j = 0;
		while( pointer[j] != '=' )
			temp[j] = pointer[j++];
		temp[j] = '\0';
		for( j = 0; j < strlen(temp); j++ )
			temp[j] = toupper(temp[j]);

		if( strcmp(temp,ovar) == 0 ) {
			pointer += strlen(temp)+1;
			free(temp);

            temp = malloc((strlen(pointer)+1) * sizeof(char));
			strcpy(temp,pointer);

			if( temp[0] == '\"' ) {
				for( j = 1; j < strlen(temp); j++ )
					temp[j-1] = temp[j];
				temp[j-1] = '\0';
				if( temp[j-2] == '\"' )
					temp[j-2] = '\0';
			}

			if( destination != NULL )
				strcpy(destination,temp);
			returncode = (int) strlen(temp)+1;
			free(temp);
			break;
		}
		free(temp);
		pointer += strlen(pointer) + 1;
	}

	free(ovar);

	return returncode;
}

int check_inf_file( void ) {
	int returncode = 0;

	if( (iVersion = find_label("Version")) == 0 ) {
		printf("\nNo Version information found");
		if( skip_version_check != 1 ) {
			error_encountered = 1;
			returncode = -1;
		}
	}

	if( (iManufacturer = find_label("Manufacturer")) == 0 ) {
		printf("\nNo Manufacturer information found");
		error_encountered = 1;
		returncode = -1;
	}

	//Searching for DestinationDirs label
	if( isWINNT == 1 ) {
		if( is64BIT == 1 )
            iDestDirs = find_label("DestinationDirs.NTIA64");
		else if( is64BIT == 0 )
			iDestDirs = find_label("DestinationDirs.NTX86");
		
		if( iDestDirs == 0 )
			iDestDirs = find_label("DestinationDirs.NT");
	}
	if( iDestDirs == 0 )
		iDestDirs = find_label("DestinationDirs");
	if( iDestDirs == 0 ) {
        printf("\nNo DestinationDirs information found");
		error_encountered = 1;
		return -1;
	}
		

	if( (iStrings = find_label("Strings")) == 0 ) {
		printf("\nNo Strings information found");
		error_encountered = 1;
		returncode = -1;
	}

	return returncode;
}

int check_environment( void ) {
	//Checking given cInfFile (INF FILE)
	if( cInfFile == NULL ) {
		printf("\nINF file was not given");
		error_encountered = 1;
		return -1;
	}
	else {
		if( _access(cInfFile,0) == -1 ) {
			printf("\nCould not access inf file: %s",cInfFile);
			error_encountered = 1;
			return -1;
		}
		if( _access(cInfFile,4) == -1 ) {
			printf("\nCould not accuire read permission for inf file: %s",cInfFile);
			error_encountered = 1;
			return -1;
		}
	}
	if( showmem == 0 && verbose_mode > 0 ) {
        printf("\nInf file: %s",cInfFile);
		linecount++;
	}

	//Checking (given) cWinDir (WINDOWS DIRECTORY)
	if( cWinDir == NULL ) {
		cWinDir = getenv("windir");
		if( cWinDir == NULL ) {
			printf("\nCould not retrieve 'windir' from environment vars\nand no windows directory was given");
			error_encountered = 1;
			return -1;
		}
	}
	if( _access(cWinDir,0) == -1 ) {
		printf("\nCould not find Windows Directory: %s", cWinDir);
		error_encountered = 1;
		return -1;
	}
	if( showmem == 0 && verbose_mode > 0 ) {
		linecount++;
		printf("\nWindows dir: %s",cWinDir);
	}

	if( isWINNT == -1 ) {
		if( getenv("windir") == NULL ) {
			printf("\nCould not find 'windir' environment var");
			printf("\nPlease use -9x or -NT argument to specify Windows type.");
			error_encountered = 1;
			return -1;
		}
		else if( getenv("windir") != NULL && getenv("os") == NULL )
			isWINNT = 0;
		else if( getenv("windir") != NULL && getenv("os") != NULL )
			isWINNT = 1;
	}
	if( showmem == 0 && verbose_mode > 0) {
		switch(isWINNT) {
			case 0:
				linecount += 2;
				printf("\nUsing Windows 9x/ME environment\n");
				break;
			case 1:
				linecount += 2;
				printf("\nUsing Windows NT/2K/XP environment\n");
				break;
		}
	}
	
	return 0;
}

int check_for_unicode( char *filename ) {
	FILE *inffile;
	int returncode = 0;
	int return_1, return_2;

	inffile = fopen(filename,"rb");
	return_1 = fgetc(inffile);
	return_2 = fgetc(inffile);
    
	if( return_1 == 0xFF && return_2 == 0xFE )
		returncode = 1;
	else if( return_1 == 0xFE && return_2 == 0xFF )
		returncode = 2;
	else if( return_1 == 0xEF && return_2 == 0xBB )
		returncode = 3;

	fclose(inffile);

	return returncode;
}

char *load_inf_into_memory( char *filename, char *memlocation ) { //This function loads the given file
	char *pointer;		//into the given memory location and returns the address of the dynamic memory
	unsigned long i = 0;
	int unicode;
	
	unicode = check_for_unicode(filename);

	if( showmem == 0 && verbose_mode > 0 ) {
		linecount++;
		if( unicode == 0 )
			printf("\nNormal text-based inf file found");
		else if( unicode == 1 )
			printf("\nUnicode text-based inf file found");
		else if( unicode == 2 )
			printf("\nBig Endian Unicode text-based inf file found");
	}
	
	lInfFile = count_inf_size(filename,unicode);
		
	lInfFile += 1; //We add one to be sure we have a char left to close the last string.
	if( showmem == 0 && verbose_mode > 1 ) {
		linecount++;
		printf("\nCalculated necessary memory: %lu bytes",lInfFile);
	}

	if( (memlocation = malloc(lInfFile*sizeof(char))) == NULL ) {
		printf("\nError allocating memory for inf file");
		error_encountered = 1;
	}
	else {
		if( showmem == 0 && verbose_mode > 1 ) {
			linecount += 2;
			printf("\nMemory used for INF file: %lu bytes\n",lInfFile);
		}
	}

	lines_InfFile = load_inf_file(filename,memlocation,unicode);
	
	pointer = &memlocation[0];					//We only need these pointer if we wanna show the contents of the
	if( showmem == 1 ) {
		for( i = 0; i <= lines_InfFile; i++ ) {
			printf("\n%s",pointer);
			pointer += strlen(pointer) + 1;
		}
	}

	return memlocation;
}

unsigned long load_inf_file(char *filename,char *memory, int unicode) {
	char temp = '\0';
	int quote = 0;
	unsigned long count = 0;
	unsigned long linecount = 0;
	FILE *file;
	char tempstring[10];

	int i = 0;

	file = fopen(filename,"rt");

	if( unicode > 0 ) {
		quote = fgetc(file);
		i = fgetc(file);
	}

	quote = 0;
	i = 0;
	while( !feof(file) ) {	//As long as the file exists.
		
		if( unicode == 2 )	//Unicode Big Endian places a ZERO BYTE before every character.
			fgetc(file);
		temp = fgetc(file);	//Get a char and compare
		if( unicode == 1 )	//Normal Unicode places a ZERO BYTE after every character.
            fgetc(file);

		if( unicode == 1 && temp == 13 ) {
			temp = fgetc(file);
			fgetc(file);
		}
		else if( unicode == 2 && temp == 13 ) {
			fgetc(file);
			temp = fgetc(file);
		}

		if( temp == ';' && quote == 0 ) {	//If it's a semicolon we ignore the rest of the line
			if( memory[count-1] == '\0' ) {
				strcpy(tempstring,"");
				for( i = 0; i < 9; i++ ) {
					
					if( unicode == 2 )
						fgetc(file);
					temp = toupper(fgetc(file));
					if( unicode == 1 )
						fgetc(file);

					tempstring[i] = temp;
					if( tempstring[i] == '\n' ) {
						tempstring[i] = '\0';
						break;
					}
				}
				tempstring[9] = '\0';
				if( strcmp(tempstring,"COPYFILES") == 0 ) {
					memory[count++] = 'C';
					memory[count++] = 'O';
					memory[count++] = 'P';
					memory[count++] = 'Y';
					memory[count++] = 'F';
					memory[count++] = 'I';
					memory[count++] = 'L';
					memory[count++] = 'E';
					memory[count++] = 'S';
				}
				else {
					if( temp != '\n' ) {
						while( temp != '\n' ) {
							if( unicode == 2 )
								fgetc(file);
							temp = fgetc(file);
							if( unicode == 1 )
								fgetc(file);

							if( temp == EOF )
								break;
						}
						//while( (temp=fgetc(file)) != '\n' );	//ignoring ...
					}
					if( memory[count-1] != '\0' && count > 0 ) {	//If we don't already have a newline,
						linecount++;								//we count the lines
						memory[count++] = '\0';						//we create one
					}
				}
			}
			else {
				if( temp != '\n' ) {
					while( temp != '\n' ) {
						if( unicode == 2 )
							fgetc(file);
						temp = fgetc(file);
						if( unicode == 1 )
							fgetc(file);

						if( temp == EOF )
							break;
					}
					//while( (temp=fgetc(file)) != '\n' );	//ignoring ...
				}
				if( memory[count-1] != '\0' && count > 0 ) {	//If we don't already have a newline,
					linecount++;								//we count the lines
				    memory[count++] = '\0';						//we create one
				}
			}
		}
		else if( temp == '\"' && quote == 0 ) {		//If we find a quotation mark we remember that
			quote = 1;								//because if we find one, we will want to copy spaces
			memory[count++] = '\"';					//and we copy the quotation mark
		}
		else if( temp == '\"' && quote == 1 ) {
			quote = 0;
			memory[count++] = '\"';
		}
		else if( (temp == ' ' || temp == '\t') && quote == 1 )	//if we already encountered a quotation mark,
			memory[count++] = temp;								//we want to record the space
		else if( (temp == ' ' || temp == '\t') && quote == 0 )	//if not, we don't record it.
			memory[count] = memory[count];						// <--- Dummy statement
		else if( temp == '\n' ) {
			if( memory[count-1] != '\0' && count > 0 ) {		//New line char gives us a signal to make a new line.
                memory[count++] = '\0';
				linecount++;									//And we count the lines, so we increase this counter
			}
			if( count > 37700 )
				count = count;
			quote = 0;
		}
		else memory[count++] = temp;							//If it's no special char, we'll just copy it.
	}
	if( memory[count-2] == '\0' )
		linecount--;
	else memory[count-1] = '\0';

	fclose(file);

	return linecount; //return lines count value after we're done
}

unsigned long count_inf_size( char *filename, int unicode ) { //With this function we do an estimate size count of the inf file
	char temp = '\0';
	unsigned long count = 0;
	int quote = 0;
//	int semicolon;
	int newline = 1;
	char tempstring[10];
	int i;
	FILE *file;

	file = fopen(filename,"rt");

	if( unicode > 0 ) {
		quote = fgetc(file);
		i = fgetc(file);
	}
    
	quote = 0;
	i = 0;
	while( !feof(file) ) {	//As long as the file exists.
		if( unicode == 2 )
			fgetc(file);
		temp = fgetc(file);	//Get a char and compare
		if( unicode == 1 )
			fgetc(file);

		if( temp == ';' && quote == 0 ) {	//If it's a semicolon we ignore the rest of the line
			if( newline == 1 ) {
				strcpy(tempstring,"");
				for( i = 0; i < 9; i++ ) {
					if( unicode == 2 )
						fgetc(file);
					temp = toupper(fgetc(file));
					if( unicode == 1 )
						fgetc(file);

					tempstring[i] = temp;
					if( tempstring[i] == '\n' )
						break;
				}
				tempstring[9] = '\0';
				if( strcmp(tempstring,"COPYFILES") == 0 ) {
					count += 9;
					newline = 0;
				}
				else {
					if( temp != '\n' ) {
						//while( (temp=fgetc(file)) != '\n' );
						while( temp != '\n' ) {
							if( unicode == 2 )
								fgetc(file);
							temp = fgetc(file);
							if( unicode == 1 )
								fgetc(file);
							if( temp == EOF )
								break;
						}
					}
					if( newline != 1 ) {
						count++;
						newline = 1;
					}
				}
			}
			else  {
				if( temp != '\n' ) {
					while( temp != '\n' ) {
						if( unicode == 2 )
							fgetc(file);
						temp = fgetc(file);
						if( unicode == 1 )
							fgetc(file);
						if( temp == EOF )
							break;
					}
                    //while( (temp=fgetc(file)) != '\n' );	//ignoring ...
				}
				if( newline != 1 ) {	//If we don't already have a newline,
					count++;								//we count the lines
				    newline = 1;
				}
			}
		}
		else if( temp == '\"' && quote == 0 ) {		//If we find a quotation mark we remember that
			quote = 1;								//because if we find one, we will want to copy spaces
			count++;
		}
		else if( temp == '\"' && quote == 1 ) {
			quote = 0;
			count++;
		}
		else if( (temp == ' ' || temp == '\t') && quote == 1 )	//if we already encountered a quotation mark,
			count++;											//we want to record the space
		else if( (temp == ' ' || temp == '\t') && quote == 0 )	//if not, we don't record it.
			count = count;										// <--- Dummy statement
		else if( temp == '\n' ) {
			if( newline != 1 ) {		//New line char gives us a signal to make a new line.
                count++;			//And we count the lines, so we increase this counter
				newline = 1;
				quote = 0;
			}
		}
		else {
			count++;
			newline = 0;//If it's no special char, we'll just copy it.
		}
	}

	fclose(file);

	return count;
}

//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* * * * Simple functions to improve the readability of the code * * * *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

//mode 0: Check for existance of directory.
//mode 1: Check for existance, if non-existant create directory.
int check_dir_existance(char *path, int mode) { //Checking if a path exists.
	int status;
	char *tempstring;
	char *temp;
	unsigned int i;
	
	status = access(path,0);

	if( status == 0 )
		return 0;
	else if( status == -1 && mode == 0 )
		return -1;
	else if( status == -1 && mode == 1 ) {		//If path does not exists and we have permission to create one.
		tempstring = malloc( (strlen(path)+1) * sizeof(char));
		if( tempstring == NULL ) {
			printf("\nCould not allocate memory for \'tempstring\'");
			error_encountered = 1;
			return -1;
		}
		for( i = 0; i < 4; i++ )
			tempstring[i] = path[i];

		for( i = 4; i < strlen(path); i++ ) { //We check every part of the path, if it does not exist, we create it.
			if( path[i] == '\\' ) {
				tempstring[i] = '\0';
				if( access(tempstring,0) == -1 ) {
					temp = malloc( (strlen(tempstring)+6) * sizeof(char) );
					strcpy(temp,"md \"");
					strcat(temp,tempstring);
					strcat(temp,"\"");
                    system(temp);
					free(temp);
				}
				tempstring[i] = '\\';
			}
			else tempstring[i] = path[i];
		}
		tempstring[i] = '\0';

		if( access(tempstring,0) == -1 ) {	//One last directory check for the last part of the path.
			temp = malloc( (strlen(tempstring)+6) * sizeof(char) );
			strcpy(temp,"md \"");
			strcat(temp,tempstring);
			strcat(temp,"\"");
            system(temp);
			free(temp);
		}
		free(tempstring); 
	}

	return 0;
}


//Find the given label in the dynamic memory: mInfFile
unsigned int find_label( char *label ) {
	char *clabel;	//Label given when calling this function
	char *olabel;	//current label (out of memory) but with capital chars.
	char *pointer;
	unsigned int returncode = 0;
	unsigned int i, j;

	clabel = malloc((strlen(label)+3)*sizeof(char));
	strcpy(clabel,"[");
	strcat(clabel,label);
	strcat(clabel,"]");

	for( i = 0; i < strlen(clabel); i++ )
		clabel[i] = toupper(clabel[i]);
    
	if( verbose_mode > 2 )
		printf("\nSearching for label: %s",label);

	pointer = &mInfFile[0];
	for( i = 0; i <= lines_InfFile; i++ ) {
		olabel = malloc( (strlen(pointer)+1) * sizeof(char));
		
		strcpy(olabel,pointer);
		for( j = 0; j < strlen(olabel); j++ )
			olabel[j] = toupper(olabel[j]);

		if( strcmp(olabel,clabel) == 0 ) {
			returncode = i+1; //We don't want the line with the label, but the line where the real info begins.
			free(olabel);
			break;
  		}

		free(olabel);
		pointer += strlen(pointer) + 1;
	}
	free(clabel);

	return returncode;
}

int get_dirid( long dirid, char *addition, char *dest_address ) {
	char path[_MAX_PATH];			//This function is to get the proper directory structure out of IDs
	int i;
	
	int count, j;
	char *add_temp;
	char *add_result;
	char *new_addition;

	if( pr_proc == PR_NODEF || pr_ver < 0 ) {
		printf("\nError, wrong printer defs");
		error_encountered = 1;
		return -1;
	}

	j = 0;	//This FOR-loop replaces %stringvar% with the real strings.
	new_addition = malloc((strlen(addition)+1)*sizeof(char));
	strcpy(new_addition,"");
	for( i = 0; i < (int) strlen(addition); i++ ) {
		if( addition[i] == '%' ) {
			i++;
			
			count = 0;
			while( addition[i+count] != '%' )
				count++;
			add_temp = malloc((count+1)*sizeof(char));
			count = 0;
			while( addition[i+count] != '%' )
				add_temp[count++] = addition[i+count];
			add_temp[count] = '\0';
			
			add_result = malloc(get_stringvar(add_temp,NULL)*sizeof(char));
			get_stringvar(add_temp,add_result);
						
			if( strlen(add_temp) < strlen(add_result) )
				new_addition = realloc(new_addition,(strlen(addition)+1+(strlen(add_result)-strlen(add_temp)))*sizeof(char));
			strcat(new_addition,add_result);
						
			i += count;
			j += (int) strlen(add_temp);
			
			free(add_temp);
			free(add_result);
		}
		else {
            new_addition[j++] = addition[i];
			new_addition[j] = '\0';
		}
	}
	new_addition[j] = '\0';
	
	//Now the right directory is being found and processed.
	if( dirid == -1 ) {
        strcpy(path, "");	//Absolute path, so no beginning is specified
	}
	else if( dirid == 1 ) {	//Directory from which the INF was installed,
        strcpy(path,cInfFile); //Not sure if this works ...
		for( i = (int) strlen(path) - 1; i > 0; i-- ) {
			if( path[i] == '\\' ) {
                path[i] = '\0';
			break;
			}
		}
	}
	else if( dirid == 10 ) {	// Windows directory (%windir%)
        strcpy(path,cWinDir);
	}
	else if( dirid == 11 ) { // System directory (%windir%\system32 for NT, %windir%\system for 9x)
		strcpy(path,cWinDir);
        
		if( isWINNT == 0 )
			strcat(path,"\\system");
		else strcat(path,"\\system32");
	}
	else if( dirid == 12 ) {	// Drivers directory (%windir%\system32\drivers for NT,
        strcpy(path, cWinDir);	// %windir%\system\iosubsys for 9x)
		if( isWINNT == 0 )
			strcat(path,"\\system\\iosubsys");
		else strcat(path, "\\system32\\drivers");
	}
	else if( dirid == 17 ) {	// inf file dir, %windir%\inf
        strcpy(path, cWinDir);
		strcat(path, "\\inf");
	}
	else if( dirid == 18 ) {	// help dir, %windir%\help
        strcpy(path, cWinDir);
		strcat(path, "\\Help");
	}
	else if( dirid == 20 ) {	// fonts dir, %windir%\fonts
        strcpy(path, cWinDir);
		strcat(path, "\\Fonts");
	}
	else if( dirid == 23 || dirid == 66003 ) {	// ICM color dir, %windir%\system32\spool\drivers\color for NT
		strcpy(path, cWinDir);
		if( isWINNT == 0 )
			strcat(path,"\\system\\color");
		else strcat(path, "\\system32\\spool\\drivers\\color");
	}
	else if( dirid == 24 ) {	// root dir of systemdisk,
        strcpy(path, cWinDir);	// %windir% without the windir :)
		path[3] = '\0';
	}
	else if( dirid == 30 ) {	//Experimental: could not find a way to find this dir
		strcpy(path, cWinDir);
		path[3] = '\0';
	}
	else if( dirid == 50 ) {	// System dir for NT-based systems %windir%\system
        strcpy(path, cWinDir);	
		strcat(path, "\\System");
	}
	else if( dirid == 51 ) {	// spool dir, %windir%\system32\spool for NT
		strcpy(path, cWinDir);
		if( isWINNT == 0 )
			strcat(path,"\\spool");
		else strcat(path, "\\system32\\spool");
	}
	else if( dirid == 52 || dirid == 66000) {	// Spool_drivers dir, %windir%\system32\spool\drivers for NT
		strcpy(path, cWinDir);
		
		if( isWINNT == 0 )
			strcat(path,"\\spool\\printers");
		else {
			strcat(path, "\\system32\\spool\\drivers\\");
			add_printdirs(path,pr_proc,pr_ver);
		}
	}
	else if( dirid == 55 || dirid == 66001) {	// printprocessor dir, %windir%\system32\spool\prtprocs for NT
		strcpy(path, cWinDir);
		if( isWINNT == 0 )
			strcat(path,"\\system\\spool\\printers");
		else {
			strcat(path, "\\system32\\spool\\prtprocs\\");
			add_printdirs(path,pr_proc,pr_ver);
		}
	}
	else if( dirid == 66002 ) {
		strcpy(path, cWinDir);
		if( isWINNT == 0 )
			strcat(path,"\\system");
		else strcat(path,"\\system32");
	}
    else strcpy(path,"__NOT_FOUND__");	//Not found?

	//if a full path is given, thus the 2nd char is a ':' then we copy the full path.
	if( new_addition[1] == ':' && dirid == -1 ) {
		strcpy(path,new_addition);
	}
	else {
		if( path[strlen(path)-1] != '\\' )	//We want to prevent to slashes don't we?
			strcat(path,"\\");
		strcat(path,new_addition);	//Adding addition to currently found path
	}
	
	if( dest_address != NULL )
		strcpy(dest_address, path);
    
	free(new_addition);

	if( strcmp(path,"__NOT_FOUND__") == 0 )
		return 0;
	else return (int) strlen(path) + 1;
}

void add_printdirs( char *deststring, int processortype, int version ) {
	char *temp;

	if( deststring[strlen(deststring)-1] != '\\' ) {
		deststring[strlen(deststring)+1] = '\0';
		deststring[strlen(deststring)] = '\\';
	}

	if( processortype == PR_NODEF )
		return;
	else if( processortype == PR_WIN4 )
		strcat(deststring,"win40\\");
	else if( processortype == PR_IA64 )
		strcat(deststring,"IA64\\");
	else if( processortype == PR_ALPHA )
		strcat(deststring,"W32ALPHA\\");
	else if( processortype == PR_X86 )
		strcat(deststring,"W32X86\\");

	temp = malloc(10*sizeof(char));
	strcpy(temp,"");
	itoa(version,temp,10);
	strcat(deststring,temp);
	free(temp);

	strcat(deststring,"\\");
}