#ifndef _inf_args_H
#define _inf_args_H

//Different versions of windows
#define VER_UNDEFINED	 0
#define VER_WIN9598ME	10
#define VER_WINNT4		20
#define VER_WIN2K		30
#define VER_WINXP		40

//Different processor architectures
#define ARC_UNDEFINED	0
#define ARC_X86			1
#define ARC_IA64		2

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct _ParsedArguments {
	char *cInffile;		//INF file
	char *cDestdir;		//Dump directory
	char *cWindir;		//Windows directory
	
	int verbose;		//How much info do we get?
	int winver;			//Which version of windows are we dealing with?
	int architecture;	
	/*
	int showmem;		//Show memory dump and quit
	int is64bit;		//Is 64Bit Windows
	int no_copy_errors;//Don't display copy-errors
	int show_files;	//Show all files that are to be copied.
	int skip_needs;	//Don't parse the needs directive
	int skip_version_check;//Skip version check, don't drop on missing version label
	int wait_after_completion;	//pause screen after finished
	int wait_on_error;	//Only pause screen if finished and errors found.
	int error_encountered;//Have we had an error?
	int label_given;
	int linecount;
	int totalfiles;
	int copiedfiles;
	*/
};

int process_arguments( int iArgCount, char **args, struct _ParsedArguments *argsmem );
void startup_config( struct _ParsedArguments *temp );
void shutdown_config( struct _ParsedArguments *temp );
int fileExists( char *filename );
int isargsign( char sign );

#endif