#ifndef _inf_version_H
#define _inf_version_H

#include "infreadbuf.h"
#include <stdio.h>

struct _VersionInformation {
	char signature[15];
	char classguid[41];
	char classname[33];
	char *provider;
	char *layout;
	char *catalog;
	char *catalog_nt;
	char *catalog_ntx86;
	char *catalog_ntia64;
	char *driverver;
};

int verRetrieveinfo( struct _VersionInformation *vinfo, char *buffer);
void verParseversionlabel(struct _VersionInformation *info, char *buffer, int location );
void shutdown_version( struct _VersionInformation *temp );

#endif