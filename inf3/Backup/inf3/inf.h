#ifndef _inf_inf_H
#define _inf_inf_H

int infLoadfile( struct _Inffile *inf );
int infGetline( FILE *file, char *buffer, int normal, int max );
int infCleanline( char *buffer );
void infRemovecomment( char *buffer );
void infRemovespaces( char* buffer );
long getFilesize( FILE *file );
void shutdown_inf( struct _Inffile *temp );

struct _Inffile {
	FILE *file;
	char *filename;
	char *buffer;
	int bufmax;
	int buflines;
};

#endif