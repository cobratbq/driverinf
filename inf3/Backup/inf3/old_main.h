#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <io.h>
#include <ctype.h>

//Because BC31 doesn't recognize '_access' we define 'access'
#if !defined(_WIN32)
	#define _access access
#endif

#define PR_NODEF	-1
#define PR_WIN4		 0
#define PR_ALPHA	 1
#define PR_X86		 2
#define PR_IA64		 3
#define PR_MAX_PROC  3

//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
//* * * * * * * * * * All globally used variables * * * * * * * *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#define MAX_EXT_RESERVATION 20

char *cDestDir;
char *cWinDir;
//Inf File Variables
char *cInfFile;				//Pointer that stores the filename-string of the inf file
unsigned long lInfFile;		//var that stores the size of the file's useful chars, needed to allocate dynamic mem
unsigned long lines_InfFile;//var that stores the amount of lines in the inf file memory copy.
char *mInfFile;				//pointer to dynamic memory that stores the inf file after being filtered.

//Manufacturer variables
unsigned int iManufacturer = 0;	//Linenumber where the Manufacturerlabel begins
char *cManufacturer;

//System variables
int isWINNT = -1;			//What directory settings do we use? NT or 9x?
int is64BIT = 0;			//Do we need to search for the NTIA64 extension?
int error_encountered = 0;
int pr_proc = PR_NODEF;
int pr_ver = 3;
int verbose_mode = 1;		//What do we need to say?
int wait_after_completion = 0;
int wait_on_error = 0;		//Wait only if an error has occurred.
int skip_version_check = 0;	//Ignore missing Version label.
int skip_needs = 0;			//Skip processing needs directive.
int showmem = 0;			//Dump memory to console
int label_given = 0;		//Continue with given label
int no_copy_errors = 0;		//Don't show copy file error messages
int show_files = 0;			//Show every file which is copied
int linecount = 0;			//Count the lines.
int totalfiles = 0;			//Total files to be copied
int copiedfiles = 0;		//Total copied files

//Labelnumber vars
unsigned int iDestDirs = 0;	//Linenumber where the DestinationDirs label begins
unsigned int iStrings = 0;	//Linenumber where the Strings label begins
unsigned int iVersion = 0;	//Linenumber where the Version label begins
unsigned int iCurrentLabel = 0;	//Linenumber where the CurrentLabel begins.

char *cCopyFiles;			//List with labels which are to be copied
char *cCopyInfs;
char *cNeeds;				//List with labels which need to be processed
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *




//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* * * * * * * * * * All global functions* * * * * * * * * * * * 
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

//INF File functions
int check_for_unicode( char *filename );
char *load_inf_into_memory( char *filename, char *memlocation );	//Load inf file into memory
unsigned long count_inf_size( char *filename, int unicode );
unsigned long load_inf_file(char *filename,char *memory, int unicode);

//General functions
int check_environment( void );					//Check settings
int check_inf_file( void );						//Do all necessary labels exist
int check_dir_existance(char *path, int mode);	//Does this directory exist? mode 1 creates dir

int manufacturer_menu( void );					//Manufacturer menu
int models_menu( char *label );					//Models menu

int process_needs( void );						//Process Needs list
int process_label(char *labelname);				//Process given label
int process_copyfiles( void );					//Process CopyFiles list.
int process_copyinfs( void );					//Process CopyInfs list.

int copy_files( char *labelname, char *destdir );	//Process label and call copy_file
int copy_file(char *filename, char *destdir, char *sourcefile ); //copies given filename to argument destdir
int copy_inffile( void );						//Copy inf file to dest-dir

int get_stringvar( char *var, char *destination );	//Get string of given variable
int get_label(int line, char *destination);		//Retrieve label from given linenumber.
int get_stringislabel(int linenumber, char *destination); //Retrieve label when (name)string is label
int get_dirid( long dirid, char *addition, char *dest_address );	//Get directory with dirid and additional path
void add_printdirs( char *deststring, int processortype, int version );
int get_destdir( char *labelname, char *destvar );	//Retrieve destination directory

void press_any_key( void );
unsigned int find_label( char *label );			//Find given label in mem-duplicate of inf file
int varstotext(char *text, char *destination);	//give text with vars and pure-text returns

//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *