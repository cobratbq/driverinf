#ifndef _inf_main_H
#define _inf_main_H


#define INF_NORMAL_BUFFER 512
#define INF_MAX_BUFFER 1024

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#include "args.h"
#include "inf.h"
#include "version.h"
#include "manufact.h"
#include "startproc.h"

#if !defined(_WIN32)
	#define _access access
#endif


#endif