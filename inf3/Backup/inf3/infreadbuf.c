#include "infreadbuf.h"
//irbFunctionname (irb = Inf Read Buffer)

int irbFindlabel( char *labelname, char *buffer ) {
	unsigned int i = 0;
	int done;
	char *search;
	char *found;
	
	//Combine info to make complete label to find...
	search = (char*)malloc((strlen(labelname)+3)*sizeof(char));
	strcpy(search,"[");
	strcat(search,labelname);
	strcat(search,"]");
	//we need to compare case-insensitive
	strtoupper(search);

	done = 0;
	while( !done ) {
		if( strlen(&buffer[i]) == 0 )
            break;
		
		//Prepare string for comparison
		found = (char*)malloc((strlen(&buffer[i])+1)*sizeof(char));
		strcpy(found,&buffer[i]);
        strtoupper(found);	//compare case-insensitive

		//if label found set done to 1
		if( strcmp(search,found) == 0 )
				done = 1;
		i += (unsigned int)strlen(&buffer[i])+1;
	}

	free(search);


	//We return -1 if we didn't find anything, if we did find anything
	//we return the location of the first line of the found label.
	if( done != 1 )
		return -1;
	else return i;
}

int irbCorrectvar( char *line, char *buffer, char *target ) {
	char *temp, *temp2;
	char *result, *begin;
	int i, j, k;
	int oldsize, newsize, compensation = 0;
	int sign = 0;

	//Prepare the begin var which is before the modifications.
	begin = (char*)malloc((strlen(line)+1)*sizeof(char));
	strcpy(begin,line);

	//While there's still a % sign, we'll be looking for vars
    while( strchr(begin,'%') ) {

		//Allocate space for the loop-result
		result = (char*)malloc((strlen(begin)+1)*sizeof(char));

        k = 0; //set result-counter to 0

		//Allocate space for begin var (which will also contain the final result)
		temp = (char*)malloc((strlen(begin)+1)*sizeof(char));
		for( i = 0; i < (int)strlen(begin); i++ ) {
			if( begin[i] == '%' ) {

				//if we found a % sign and it's the first
				if( sign == 0 ) {
					//record finding of sign
					sign = 1;
					//set temp memory counter to 0
					j = 0;
				}
				else {
					//record char to temp memory (to collect entire var-name)
					temp[j] = '\0';
					oldsize = (int)strlen(temp)+2;

					//Search the stringvar and return it's original size
					newsize = irbFindstringvar(temp,buffer,NULL);
					//Allocate memory for new text size
					temp2 = (char*)malloc((newsize+1)*sizeof(char));
					//write new text to temp2 var
					irbFindstringvar(temp,buffer,temp2);
					
					//reallocate result space to fit new text in result var
					result = realloc(result,(strlen(line)+(newsize-oldsize)+compensation+1)*sizeof(char));
                    result[k++] = '\0';
					//add the new text to the other resulting texts...
					strcat(result,temp2);
					
					//Free second temp variable (which contained the full text of the var)
                    free(temp2);

					//Calculate a compensationnumber to compensate when there are more vars.
					compensation += (newsize-oldsize);

					//reset sign to 0, we're looking for a new one.
					sign = 0;
					k = (int)strlen(result);
				}
			}
			else if( sign == 1 )
				//we have already found a sign,
				//so we collect the next chars in a separate var so we can use it
				//to look for the varstring.
				temp[j++] = begin[i];

			else if( sign == 0 )
				//This is normal text, just write it to the resulting string.
				result[k++] = begin[i];
		}
		//Close the resulting string.
		result[k] = '\0';

		free(temp);

		//Write the resulting string to begin...
		//Now we go through the loop again, to filter out the remaining vars.
		//(This is only used to filter out nested vars)
		begin = realloc(begin,(strlen(result)+1)*sizeof(char));
		strcpy(begin,result);
		free(result);
	}
	
	//Record final size
	newsize = (int)strlen(begin);
	//if target is given we copy the final string to target
	if( target != NULL )
		strcpy(target,begin);
	free(begin);

	//Return new size
	return newsize;
}
//Check for '=' inside quote!
int irbFindstringvar( char *stringvarname, char *buffer, char *target ) {
	int location = -1, pos = -1;
	int size = -1;
	char *line;
	char *search;
    
	//Prepare strinvarname variable to be compared case-insensitive
	search = (char*)malloc((strlen(stringvarname)+1)*sizeof(char));
	strcpy(search,stringvarname);
	strtoupper(search);

	location = irbFindlabel("Strings",buffer);

	if( location == -1 )
		return 0;

	while( buffer[location] != '[' && strlen(&buffer[location]) > 0 ) {

		line = (char*)malloc((strlen(&buffer[location])+1)*sizeof(char));
		strcpy(line,&buffer[location]);

		if( (pos = splitString(line,'=')) > -1 ) {

			//toUpper the first string part
			strtoupper(line);
			if( strcmp(search,line) == 0 ) {
				//Remove the front quote-char
				if( line[pos] == '\"' )
					strcpy(&line[pos],&line[pos+1]);
				//Remove the trailing quote-char
				if( line[pos+strlen(&line[pos])-1] == '\"' )
					line[pos+strlen(&line[pos])-1] = '\0';
				
				//If a targetpointer is given, copy to target
				if( target != NULL )
					strcpy(target,&line[pos]);
				
				//Record the size to return in the end
				size = (int)strlen(&line[pos]);

				free(line);
				break;
			}
		}
	
		free(line);

		location += ((int)strlen(&buffer[location])+1);
	}

	free(search);

	return size;
}

int splitString( char *line, char sign ) {
	int pos = -1;
	int i;

	for( i = 0; i < (int)strlen(line); i++ ) {
		if( line[i] == sign ) {
			line[i] = '\0';
			pos = i+1;
			break;
		}
	}

	return pos;
}
void strtoupper( char *string ) {
	int i;
	for( i = 0; i < (int)strlen(string); i++ )
		string[i] = toupper(string[i]);
}