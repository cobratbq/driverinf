#include "manufact.h"

int listLabelcontents( char *labelname, char *buffer ) {
	int location, pos;
	int counter = 0;
	char *line;
	char *name;
	char *args;

    location = irbFindlabel(labelname,buffer);
	if( location == -1 )
		return 0;

	//The same (partially) code is used in chooseLabelcontents
	while( buffer[location] != '[' && strlen(&buffer[location]) > 0 ) {
		//store current textline in line
		line = (char*)malloc((strlen(&buffer[location])+1)*sizeof(char));
		strcpy(line,&buffer[location]);
		//split on '=' sign. This separates description and labelname
		pos = splitString(line,'=');

		//We allocate some memory to store the description
		name = (char*)malloc((irbCorrectvar(line,buffer,NULL)+1)*sizeof(char));
		irbCorrectvar(line,buffer,name);	//and we filter out any stringvars.

		//we move the label to the beginning of the var's memory
		strcpy(line,&line[pos]);
		pos = splitString(line,',');	//we split on ',' to separate label from extra versioninfo
		args = (char*)malloc((strlen(&line[pos])+1)*sizeof(char)); //allocate space for extra versioninfo
		strcpy(args,&line[pos]);	//copy extra versioninfo to args variable.
		        
        printf("\n%d. %s  -=-  %s (%s)",counter++,name,line,args);

		free(args);
		free(name);
		free(line);
		
		location += (int)strlen(&buffer[location])+1;
	}

	return 1;
}

//There's mostly a size of 512 chars used to store label+args... this could give some problems
int chooseLabelcontents( int choice, char *labelname, char *buffer, char *targetstorage ) {
	int location, pos, counter = 0;
	char *line, *name, *args;
	int returncode = 0;

	location = irbFindlabel(labelname,buffer);
	if( location == -1 )
		return 0;

	while( buffer[location] != '[' && strlen(&buffer[location]) > 0 ) {
		//store current textline in line
		line = (char*)malloc((strlen(&buffer[location])+1)*sizeof(char));
		strcpy(line,&buffer[location]);
		//split on '=' sign. This separates description and labelname
		pos = splitString(line,'=');

		//We allocate some memory to store the description
		name = (char*)malloc((irbCorrectvar(line,buffer,NULL)+1)*sizeof(char));
		irbCorrectvar(line,buffer,name);	//and we filter out any stringvars.

		//we move the label to the beginning of the var's memory
		strcpy(line,&line[pos]);
		pos = splitString(line,',');	//we split on ',' to separate label from extra versioninfo
		args = (char*)malloc((strlen(&line[pos])+1)*sizeof(char)); //allocate space for extra versioninfo
		strcpy(args,&line[pos]);	//copy extra versioninfo to args variable.
		
		if( counter == choice ) {
            //If we've found the right label, we copy it together with its args to the storagestring, and quit the function.
			strcpy(targetstorage,line);
			strcat(targetstorage,",");
			strcat(targetstorage,args);

            free(args);
			free(name);
			free(line);

			returncode = 1;
			break;
		}

		free(args);
		free(name);
		free(line);

		counter++;

		location += (int)strlen(&buffer[location])+1;
	}
	
	return returncode;
}

//This function is used to create the label that we need to use.
//For win9x systems this is just the label, for NT systems this is the label,
//with (most of the time) a certain suffix like .NT or .NTX86 or even with a NT version number.
int parseLabel( char *label_with_arguments, struct _ParsedArguments *args, char *buffer ) {
	char *label;
	char *arguments;
	char *foundlabel;
	int pos;
	int error = 0;

	if( args->winver == VER_UNDEFINED )
		return 0;

	foundlabel = (char*)malloc((INF_NORMAL_BUFFER+1)*sizeof(char));
	strcpy(foundlabel,"");

	pos = splitString(label_with_arguments,',');
	label = (char*)malloc((INF_NORMAL_BUFFER+1)*sizeof(char));
	strcpy(label,label_with_arguments);
        
	if( args->winver == VER_WINNT4 || args->winver == VER_WIN2K ) {
		
		//Check architecture specific labels first...
		if( args->architecture != ARC_UNDEFINED ) {
			switch( args->architecture ) {
				case ARC_X86:
					strcat(label,".NTX86");
					break;

				case ARC_IA64:
					strcat(label,".NTIA64");
					break;

				case ARC_UNDEFINED:
				default:
					strcat(label,".NTX86");
					break;
			}
			
			//Does an architecture-specific label exist?
			if( irbFindlabel(label,buffer) != -1 && !error )
				strcpy(foundlabel,label);
		}

		//Only look further if no valid label is found yet.
		if( strlen(foundlabel) == 0 ) {
			strcpy(label,label_with_arguments);
			strcat(label,".NT");
			if( irbFindlabel(label,buffer) != -1 && !error )
				strcpy(foundlabel,label);
		}

		//if we can't find an architecture/os specific label,
		//we use the standard label. And so we exit.
	}
	else if( args->winver == VER_WINXP ) {
	}
	else strcpy(foundlabel,label);

	if( strlen(foundlabel) > 0 )
		strcpy(label_with_arguments,foundlabel);
	else strcpy(label_with_arguments,label);

	free(label);
	free(foundlabel);
	//free(arguments);

	if( error )
		return 0;
	else return 1;
}