#ifndef _inf_manufact_H
#define _inf_manufact_H

#include <stdio.h>
#include "infreadbuf.h"
#include "args.h"
#include "main.h"

int listLabelcontents( char *labelname, char *buffer );
int chooseLabelcontents( int choice, char *labelname, char *buffer, char *targetstorage );
int parseLabel( char *label_with_arguments, struct _ParsedArguments *args, char *buffer );

#endif