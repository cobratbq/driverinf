/**
*
*  BKEYBRD.H	Header file for BIOS keyboard functions for Turbo C TOOLS
*
*  Version	6.00 (C)Copyright Blaise Computing Inc.  1986,1987,1989
*
**/

#ifndef DEF_BKEYBRD		/* Prevent redefinition.	    */
#define DEF_BKEYBRD 1		/* Prevent second reading of these  */
				/* definitions. 		    */

#include <ctype.h>
#include <butil.h>

#define KB_BIOS_INT 0x16	/* General BIOS keyboard gate.	    */


#define KB_HEAD     0		/* KBSTUFF, KBPLACE: Whether to     */
				/* place char(s) at HEAD or at	    */
#define KB_TAIL     1		/* TAIL of queue.		    */

				/* KBPLACE error returns:	    */
#define KB_OK	    0		/*   no error,			    */
#define KB_FULL     1		/*   buffer full,		    */
#define KB_PLACE    2		/*   bad "where" specification.     */

#define KB_DATASEG  0x40	/* Segment value of BIOS keyboard   */
				/* data.			    */
#define KB_HEADLOC  0x1a	/* Location of queue head pointer.  */
#define KB_TAILOC   0x1c	/* Location of queue tail pointer.  */
				/*				    */
#define KB_SHIFTLOC 0x17	/* Location of shift status register*/
				/*				    */
#define KB_BUFEND   0x3e	/* Address of keyboard buffer end.  */
#define KB_BUFSTART 0x1e	/* Address of keyboard buffer start.*/
				/*				    */
				/* Size of buffer in bytes.	    */
#define KB_BUFSIZE    (KB_BUFEND - KB_BUFSTART)
				/*				    */
				/* Size of buffer in keystrokes.    */
				/*				    */
#define KB_BUFACSIZE  (((KB_BUFEND - KB_BUFSTART) >> 1) - 1)
				/* Far pointer to buffer head word. */
#define KB_BUFHEADADDR (uttofaru (KB_DATASEG, KB_HEADLOC))
				/*				    */
				/* Far pointer to buffer tail word. */
#define KB_BUFTAILADDR (uttofaru (KB_DATASEG, KB_TAILOC))
				/*				    */
				/* Far pointer to shift status byte.*/
#define KB_SHIFTADDR (uttofaru (KB_DATASEG, KB_SHIFTLOC))

#define KB_NOEXTENDED	0	/* Values for b_kbxten. 	    */
#define KB_EXTENDED	1

#define KB_NOENHANCED	0	/* Values for b_kbnhan. 	    */
#define KB_ENHANCED	1

#define KB_USE_NORMAL	0	/* Values for b_kbusex. 	    */
#define KB_USE_EXTEND	1
#define KB_ERROR      (-1)	/* Additional value returned by     */
				/*   KBEXTEND.			    */

#define KB_NO_KEY_FOUND 0	/* Values for the key_found field   */
#define KB_KEY_FOUND	1	/*   of KB_DATA.		    */

		/* Values to be placed in the control_action field  */
		/* of KB_DATA.					    */
#define KB_NO_REMOVE_KEY    0	/* Do not remove key from buffer.   */
#define KB_REMOVE_KEY	    1	/* Remove key from head of buffer.  */
#define KB_FLUSH	    2	/* Flush the keyboard buffer.	    */


typedef struct kstatus		/* KEYSTATUS structure: 	    */
{				/* Maps keyboard status register    */
				/* onto unsigned integer.	    */
   unsigned right_shift  : 1;	/* Right Shift key depressed.	    */
   unsigned left_shift	 : 1;	/* Left  Shift key depressed.	    */
   unsigned ctrl_shift	 : 1;	/* Ctrl        key depressed.	    */
   unsigned alt_shift	 : 1;	/* Alt	       key depressed.	    */
   unsigned scroll_state : 1;	/* Scroll Lock has been toggled.    */
   unsigned num_state	 : 1;	/* Num Lock has been toggled.	    */
   unsigned caps_state	 : 1;	/* Caps Lock has been toggled.	    */
   unsigned ins_state	 : 1;	/* Insert state is active.	    */
				/*				    */
   unsigned filler	 : 3;	/* Filler for word alignment.	    */
   unsigned hold_state	 : 1;	/* Suspend key has been toggled.    */
   unsigned scroll_shift : 1;	/* Scroll Lock key depressed.	    */
   unsigned num_shift	 : 1;	/* Num Lock    key depressed.	    */
   unsigned caps_shift	 : 1;	/* Caps Lock   key depressed.	    */
   unsigned ins_shift	 : 1;	/* Insert      key depressed.	    */
} KEYSTATUS;


typedef struct			/* KEY_SEQUENCE structure:	    */
{				/*  character & key codes for a key.*/
    unsigned char   character_code;
    unsigned char   key_code;
} KEY_SEQUENCE;


typedef struct			/* KB_DATA structure:		    */
{				/*   information for key control    */
				/*   function.			    */
    int key_found;		/* Key was pressed if equal to	    */
				/*   KB_KEY_FOUND.		    */
    KEY_SEQUENCE key_seq;	/* Character and scan codes of key  */
				/*   (if key_found is KB_KEY_FOUND).*/
    void *pfunction_data;	/* Pointer to user-supplied	    */
				/*   information structure.	    */
    int control_action; 	/* Special action for key control   */
				/*   function to take.		    */
    int returned_action;	/* Action to be taken on return     */
				/*   from key control function.     */
				/*   Either KB_REMOVE_KEY if the    */
				/*   caller should remove the	    */
				/*   detected keystroke (if there   */
				/*   was one), or KB_NO_REMOVE_KEY  */
				/*   otherwise. 		    */
} KB_DATA;


typedef void (*PKEY_CONTROL)	/* PKEY_CONTROL: address of key     */
		   (KB_DATA *); /*   control function  -- pointer   */
				/*   to function returning void,    */
				/*   accepting a pointer to KB_DATA.*/


/* Global variables						    */

extern unsigned char b_keycod [];

extern int b_kbxten;	    /* Presence of extended BIOS functions. */

extern int b_kbnhan;	    /* Presence of enhanced keyboard.	    */

extern int b_kbusex;	    /* Flag controlling whether to use	    */
			    /* normal or extended keyboard functions*/

extern PKEY_CONTROL b_key_ctrl;     /* Default key control procedure*/
				    /* for user input functions.    */

/* Function declarations.					    */

		/* Equipment options.				    */
int   cdecl kbequip (void);		/* Sense extended BIOS and  */
					/*   enhanced keyboard.     */
int   cdecl kbextend (int);		/* Specify extended or	    */
					/*   traditional services.  */

		/* Normal BIOS operations.			    */
int   cdecl kbready (char *, int *);	/* Check for a keystroke.   */
char  cdecl kbquery (char *, int, int *,/* Get a string from CON.   */
		     int *);		/*			    */
int   cdecl kbgetkey (int *);		/* Await & return char and  */
					/*   scan code via BIOS.    */
int   cdecl kbflush (void);		/* Flush keyboard buffer.   */
int   cdecl kbqueue (int *);		/* Total and # remaining in */
					/*   keyboard queue.	    */

		/* Key control functions.			    */
int   cdecl kbpoll (PKEY_CONTROL,void *,/* Poll keyboard once for a */
		    KEY_SEQUENCE *,	/*   keystroke. 	    */
		    int);		/*			    */
KEY_SEQUENCE cdecl kbwait(PKEY_CONTROL, /* Await a keystroke using  */
		    void *);		/*   polling.		    */
void  cdecl kbkcflsh (PKEY_CONTROL,	/* Call key control function*/
		      void *);		/*   to flush keyboard	    */
					/*   buffer.		    */

		/* Controlling keyboard.			    */
void  cdecl kbset (const KEYSTATUS *);	/* Set shift status.	    */
char *cdecl kbstuff (int, char *);	/* Stuff string into queue. */
int   cdecl kbplace (int, char, char);	/* Place one key into queue.*/


#define kbstatus(pkeybd) ((*((int *) (pkeybd))) = utpeekw (KB_SHIFTADDR))

#define kbscanof(c) (isascii ((int) c)				     \
		    ? ((int) b_keycod [((int) (c))])		     \
		    : (-1))

#define kbcharof(c) (isascii ((int) c)				     \
		    ? ((int) (c))				     \
		    : (-1))


#endif				/* Ends "#ifndef DEF_BKEYBRD"       */
