/**
*
*   BMEM.H -- Header file for TURBO C TOOLS Memory Management Functions
*
*   Version 6.00 (C)Copyright Blaise Computing Inc.  1986,1987,1989
*
**/

#ifndef DEF_BMEM		      /* Prevent redefinition.	      */

typedef struct		      /* MEMCTRL:  DOS memory control block   */
{			      /*   structure.			      */
			      /*				      */
    char     _dummy;	      /* Dummy to force word alignment of     */
			      /*   unsigned members, so no waste      */
			      /*   space appears within structure.    */
			      /*				      */
			      /* Actual 16-byte copy of control block */
			      /* starts here:			      */
			      /*				      */
    char     ident;	      /* Identifying signature. 	      */
    unsigned owner_psp;       /* Segment address of owner process's   */
			      /*   PSP. 			      */
    unsigned size;	      /* Size of memory block in paragraphs.  */
    char     _reserved[11];
} MEMCTRL;

    /* Function declarations					      */

unsigned cdecl mmsize(void);	      /* Report program size.	      */
				      /*			      */
int cdecl mmctrl(unsigned,MEMCTRL *,  /* Fetch memory control block   */
	   unsigned *); 	      /* and advance to next.	      */
				      /*			      */
unsigned cdecl mmfirst(void);	      /* Report first DOS memory      */
				      /* block. 		      */

#define DEF_BMEM  1		      /* Prevent second reading of    */
				      /* these definitions.	      */

#endif				      /* Ends "#ifndef DEF_BMEM"      */
