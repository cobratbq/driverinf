/**
*
*  BLAISE.H   Header file for all Turbo C TOOLS functions
*
*  Version    6.00  (C)Copyright Blaise Computing Inc.	1987, 1989
*
**/

#include <bedit.h>		/* ED:	Field editng functions.     */
				/* Also includes BKEYBRD.H,	    */
				/* BSCREENS.H, BWINDOW.H.	    */
				/* BKEYBRD.H includes CTYPE.H and   */
				/* BUTIL.H; BUTIL.H includes	    */
				/* STRING.H and STDLIB.H.	    */

#include <bfiles.h>		/* FL:	File handling functions.    */
				/* Also includes BUTIL.H.	    */

#include <bhelp.h>		/* HL:	Help system functions.	    */
				/* Also includes BWINDOW.H.	    */

#include <binterv.h>		/* IV:	Intervention code functions.*/

#include <bintrupt.h>		/* IS:	Interrupt service functions.*/
				/* Also includes BUTIL.H, STDLIB.H, */
				/* BMEM.H.			    */

#include <bkeybrd.h>		/* KB:	Direct keyboard functions.  */
				/* Also includes CTYPE.H, BUTIL.H.  */

#include <bkeys.h>		/*	Keyboard scan and character.*/
				/*	code definitions.	    */

#include <bmenu.h>		/* MN:	Highlight bar menu	    */
				/* functions. Also includes	    */
				/* BKEYBRD.H, BWINDOW.H.	    */

#include <bmouse.h>		/* MO:	Mouse interface functions.  */
				/* Also includes BKEYBRD.H,	    */
				/* BINTRUPT.H.			    */

#include <bprint.h>		/* PR:	Printer control functions   */
				/* Also includes BIOS.H.	    */

#include <bstrings.h>		/* ST:	String functions.	    */

#include <bvideo.h>		/* VI:	Direct video functions	    */
				/* Also includes STRING.H,	    */
				/* BSCREENS.H.			    */
