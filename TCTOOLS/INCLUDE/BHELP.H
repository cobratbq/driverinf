/**
*
*  BHELP.H	Header file for Turbo C TOOLS Help Functions
*
*  Version	6.00 (C)Copyright Blaise Computing Inc.  1988
*
**/


#ifndef DEF_BHELP		  /* Prevent redefinition.	    */
#define DEF_BHELP   1

#include <bwindow.h>


#define HL_ID_SIZE	    13
#define HL_SIGN_SIZE	     7


/********************************************************************/
/* Definitions of data types.					    */
/********************************************************************/

		/* The types HL_WINDOW and HL_RECORD_DATA MUST	    */
		/* match each other field for field from view_top   */
		/* to highlight_back, inclusive.		    */

typedef struct
{
    int  view_top;		/* Top edge of viewport.	    */
    int  view_left;		/* Left edge of viewport.	    */
    int  view_bottom;		/* Bottom edge of viewport.	    */
    int  view_right;		/* Right edge of viewport.	    */
    int  origin_row;		/* Data area coordinates initially  */
    int  origin_col;		/*   appearing in viewport's origin.*/
    int  data_fore;		/* Data area foreground color.	    */
    int  data_back;		/* Data area background color.	    */
    int  border_type;		/* Viewport border type.	    */
    int  border_fore;		/* Border foreground color.	    */
    int  border_back;		/* Border background color.	    */
    int  title_type;		/* Window title.		    */
    int  title_fore;		/* Title Foreground.		    */
    int  title_back;		/* Title Background.		    */
    int  data_rows;		/* # rows in image.		    */
    int  data_columns;		/* # columns in image.		    */
    int  xref_fore;		/* Foreground/background attributes */
    int  xref_back;		/*   for cross reference items.     */
    int  highlight_fore;	/* Foreground/background attributes */
    int  highlight_back;	/*   for highlight bar. 	    */
    char *pwindow_title;	/* Text of window title.	    */
} HL_WINDOW;

/**
*
*   Database structure: the actual format of the help database is as
*   follows:
*
*   +----------------+ \
*   |		     |	|
*   | HL_FILE_HEADER |	 >  Required file header record.
*   |		     |	|
*   +----------------+ /
*
*   +----------------+ \
*   |		     |	|
*   | HL_RECORD_DATA |	|
*   |		     |	|
*   +----------------+	|
*   +----------------+	|
*   |		     |	|   0 or more help window records with
*   |  Screen Image  |	 >  compressed screen images and optional
*   |		     |	|   window titles.
*   +----------------+	|
*   ..................	|
*   :		     :	|
*   :  Window Title  :	|
*   :		     :	|
*   :................: /
*
*	    :
*	    :
*
*   +----------------+ \
*   |		     |	|
*   | HL_INDEX_DATA  |	|
*   |		     |	|
*   +----------------+	|   Index records with optional description
*   ..................	 >  strings.  Each help window record has
*   :		     :	|   exactly one index record associated with it.
*   :  Description   :	|
*   :	 String      :	|
*   :		     :	|
*   :................: /
*
*	    :
*	    :
*
**/


typedef struct
{
    char	  file_sign[20];    /* Identifying text.	    */
    char	  text_terminator;  /* Control Z (#26). 	    */
    char	  reserved;	    /* Reserved.		    */
    unsigned	  version;	    /* database version number.     */
    unsigned long index_offset;     /* Position of the index.	    */
    unsigned long file_size;	    /* Size of the file.	    */
} HL_FILE_HEADER;


#define HL_WINDOW_RECORD    0
#define HL_INDEX_RECORD     1

#define HL_FILE_SIGN	    "Blaise Computing Inc"
#define HL_VERSION	    0x0100

typedef struct
{
    unsigned type;		    /* Type of database record.     */
    unsigned length;		    /* Length of following record.  */
} HL_RECORD_ID;


typedef struct
{
    HL_RECORD_ID record_sign;	/* Record header.		    */
    char id_string[HL_ID_SIZE]; /* ID of help record.		    */
    char     reserved;		/* Reserved.			    */
    int      view_top;		/* Top edge of viewport.	    */
    int      view_left; 	/* Left edge of viewport.	    */
    int      view_bottom;	/* Bottom edge of viewport.	    */
    int      view_right;	/* Right edge of viewport.	    */
    int      origin_row;	/* Data area coordinates initially  */
    int      origin_col;	/*   appearing in viewport's origin.*/
    int      data_fore; 	/* Data area foreground color.	    */
    int      data_back; 	/* Data area background color.	    */
    int      border_type;	/* Viewport border type.	    */
    int      border_fore;	/* Border foreground color.	    */
    int      border_back;	/* Border background color.	    */
    int      title_type;	/* Window title.		    */
    int      title_fore;	/* Title Foreground.		    */
    int      title_back;	/* Title Background.		    */
    int      data_rows; 	/* # rows in image.		    */
    int      data_columns;	/* # columns in image.		    */
    int      xref_fore; 	/* Foreground/background attributes */
    int      xref_back; 	/*   for cross reference items.     */
    int      highlight_fore;	/* Foreground/background attributes */
    int      highlight_back;	/*   for highlight bar. 	    */
    unsigned title_length;	/* Window title length. 	    */
    unsigned image_length;	/* Screen image length. 	    */
} HL_RECORD_DATA;


typedef struct
{
    HL_RECORD_ID  record_sign;	    /* Record header.		    */
    char	  id_string[HL_ID_SIZE]; /* ID of help record.	    */
    char	  reserved;	    /* Reserved.		    */
    unsigned long help_record;	    /* File offset of help record.  */
    unsigned long xref_list;	    /* Cross reference list offset. */
    unsigned	  description_len;  /* Length of decription string. */
    unsigned long next_index;	    /* Offset of next index record. */
} HL_INDEX_DATA;


typedef struct help_index_node
{
    HL_INDEX_DATA	    index_data;
    char		   *pdescription;
    struct help_index_node *pleft;
    struct help_index_node *pright;
} HL_INDEX_NODE;


/********************************************************************/
/* User Macros. 						    */
/********************************************************************/

		/* Option codes for HLREAD and HLLOOKUP.	    */
#define HL_CHARS_ONLY	    CHARS_ONLY
#define HL_CHAR_ATTR	    CHAR_ATTR
#define HL_COMPRESSED	    0x40
#define HL_REMOVE_WIN	    0x10
#define HL_DESTROY_WIN	    0x30
#define HL_KBIGNORE	    (WN_KBIGNORE)
#define HL_USE_MOUSE	    (WN_USE_MOUSE)
#define HL_NO_MOUSE	    (WN_NO_MOUSE)

#define HL_TEXT_TYPE	(HL_CHARS_ONLY | HL_CHAR_ATTR | HL_COMPRESSED)


/********************************************************************/
/* Global variables.						    */
/********************************************************************/

extern HL_WINDOW      b_def_help_win;
extern char	      b_help_path[];
extern HL_INDEX_NODE *b_phelp_index;


/********************************************************************/
/* Error codes. 						    */
/********************************************************************/

#define HL_NO_ERROR	    (WN_NO_ERROR)
#define HL_NO_MEMORY	    (WN_NO_MEMORY)
#define HL_ILL_DIM	    (WN_ILL_DIM)
#define HL_NO_EXIT	    (WN_NO_EXIT)
#define HL_BAD_OPT	    (WN_BAD_OPT)
#define HL_NO_PATH	    200
#define HL_NO_HANDLES	    201
#define HL_BAD_DATABASE     202
#define HL_NO_INDEX	    203
#define HL_NOT_FOUND	    204
#define HL_VER_MISMATCH     205
#define HL_SYSTEM	    206


/********************************************************************/
/* User function declarations.					    */
/********************************************************************/

int	  cdecl hlopen(const	/* Open a help database and	    */
		       char *); /* construct its index in memory.   */
				/*				    */
int	  cdecl hlclose(void);	/* Close the help database.	    */
				/*				    */
WN_EVENT *cdecl hlread( 	/* Display a help window and accept */
		  BWINDOW *,	/* user responses.		    */
		  const 	/*				    */
		    HL_WINDOW *,/*				    */
		  const char *, /*				    */
		  int,		/*				    */
		  WN_EVENT *,	/*				    */
		  int); 	/*				    */
				/*				    */
int	  cdecl hllookup(	/* Look up the help text and window */
		  const char *, /* information for a help record.   */
		  const char *, /*				    */
		  unsigned long,/*				    */
		  HL_WINDOW  *, /*				    */
		  char **,int *,/*				    */
		  int); 	/*				    */
				/*				    */
WN_EVENT *cdecl hldisp (	/* Look up help info, display it in */
		  const char *, /* a help window and accept user    */
		  const char *, /* responses.			    */
		  unsigned long,/*				    */
		  WN_EVENT *,	/*				    */
		  int); 	/*				    */
				/*				    */


/********************************************************************/
/* Internal function declarations and macros.			    */
/********************************************************************/

void	  cdecl hlfrindx(HL_INDEX_NODE *);
int	  cdecl hlpas2c(const char *, char *, int);

#define hlerror(error)	wnerror(error)
#define hlreterr(error) wnreterr(error)


#endif				/* Ends "#ifndef DEF_BHELP"         */
