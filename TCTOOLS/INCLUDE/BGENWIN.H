/**
*
*  BGENWIN.H	 Header file to suppress support of the compiler's native
*		 text window.
*
*  Version 6.00  (C)Copyright Blaise Computing Inc.  1989
*
**/

#ifndef DEF_BGENWIN		/* Prevent redefinition.	     */
#define DEF_BGENWIN 1

		/* The symbol B_NATIVE_WINDOWS is used at compile    */
		/* time to determine whether Blaise C TOOLS routines */
		/* will support the compiler's native text window.   */
		/* If it is defined as 1, the native text window is  */
		/* supported; a value of 0 means it is not.	     */

#define B_NATIVE_WINDOWS 0	 /* No native text window support.   */
/* #define B_NATIVE_WINDOWS 1 */ /* Native text window support.      */

#endif				/* Ends "#ifndef DEF_BGENWIN".       */
