/**
*
*  BINTRUPT.H	Header file for Turbo C TOOLS Interrupt Service Functions
*
*  Version 6.00  (C)Copyright Blaise Computing Inc.  1986,1987,1989
*
**/

#ifndef DEF_BINTRUPT		      /* Prevent redefinition.	      */
#define DEF_BINTRUPT	 1

#include <stdlib.h>		      /* For size_t.		      */
#include <butil.h>		      /* For ALLREG.		      */
#include <bmem.h>		      /* For mmsize().		      */

typedef struct			      /* ISRMSG:  structure for       */
{				      /* passing messages between ISR */
				      /* and the dispatcher.	      */
				      /*			      */
    int      exit_style;	      /* Exit style:  see possible    */
				      /* values below.		      */
				      /*			      */
    unsigned working_flags;	      /* Working value of flags       */
				      /* register.  This will be      */
				      /* be propagated to an ISR's    */
				      /* caller if the ISR returns    */
				      /* without doing an IRET.       */
} ISRMSG;			      /*			      */

				      /* Exit styles from an ISR:     */
				      /*			      */
#define IEXIT_NORMAL	0	      /* Normal:  return to caller    */
				      /* via IRET		      */
				      /*			      */
#define IEXIT_RETF	1	      /* Special:  return via far     */
				      /* RETurn 		      */

#define ISRCTRL   struct isr_control  /* ISRCTRL:  ISR control block  */
struct isr_control		      /*			      */
{				      /*			      */
    unsigned fcall_opcode;	      /* NOP + Far call opcode	      */
				      /*  (0x9A90)		      */
    void (far *isrdisp)();	      /* Address of ISR dispatcher    */
    unsigned iret_opcode;	      /* IRET + RETF opcodes (0xcbcf) */
				      /*			      */
    char far *isrstk;		      /* Beginning of space allocated */
				      /*  for ISR stack(s).	      */
    unsigned isrstksize;	      /* ISR stack size 	      */
    unsigned isrsp;		      /* ISR stack pointer value at   */
				      /*  start of current ISR call   */
				      /*			      */
    unsigned isrds;		      /* DS value required by ISR     */
    unsigned isres;		      /* ES value required by ISR     */
    void (far *isr)(ALLREG *,	      /* Address of ISR itself	      */
		    ISRCTRL *,	      /*			      */
		    ISRMSG *);	      /*			      */
    unsigned isrpsp;		      /* PSP of program containing ISR*/
				      /*			      */
    void far *prev_vec; 	      /* Previous value of vector     */
				      /*			      */
    unsigned level;		      /* Number of calls in progress  */
				      /*  (0 if not in use)	      */
    unsigned limit;		      /* Maximum number of nested     */
				      /*  calls 		      */
    unsigned signature; 	      /* Signature identifying this   */
				      /*  structure		      */
    unsigned sign2;		      /* One's complement of          */
				      /*  "signature"                 */
    char     ident[16]; 	      /* Identifying name	      */
    unsigned control;		      /* Bit fields to control	      */
				      /*  dispatcher options	      */
    unsigned status;		      /* Status info left by	      */
				      /*  dispatcher		      */
    char     scratch[10];	      /* Scratch space for use by     */
				      /*  dispatcher & related	      */
				      /*  programs		      */
};

    /* Offsets of various items in an ISRCTRL structure.	      */

#define ICB_ENTRY_OFFSET    0	      /* Address to install in	      */
				      /* interrupt vector.	      */
#define ICB_PSP_OFFSET	   24
#define ICB_PREV_OFFSET    26
#define ICB_SGN_OFFSET	   34
#define ICB_S2_OFFSET	   36
#define ICB_IDENT_OFFSET   38

    /* Control bits in ISRCTRL structure.			      */

#define ICB_NOCHECK	    1	      /* Don't adjust stack limit.    */

#define ICB_SIGNATURE	(('0' << 8) | 'B')  /* "B0"                   */

typedef struct			      /* DEV_HEADER:  Device header   */
{				      /* structure available to       */
				      /* DOS critical error (INT 0x24)*/
				      /* handlers.		      */
				      /*			      */
    void far *next_dev; 	      /* Physical address of next     */
				      /* device header in chain       */
				      /* (0xffff:0xffff if end)       */
				      /*			      */
    unsigned attr;		      /* Attribute bits 	      */
				      /*			      */
    unsigned strategy;		      /* Offset of "strategy" routine */
				      /*			      */
    unsigned service;		      /* Offset of service routine    */
				      /*			      */
    char     name[8];		      /* If character device, this is */
				      /* its name; if block device,   */
				      /* name[0] is the number of     */
				      /* drives served by this driver.*/
} DEV_HEADER;

    /* Symbols for use with ISCURPRC				      */

#define  IS_SETPROC  1	      /* Set currently executing process.     */
#define  IS_RETPROC  0	      /* Return currently executing process.  */

    /* Error codes.						      */

#define  IS_OK		  0   /* No error detected.		      */
#define  IS_NO_MEMORY	  1   /* Insufficient memory.		      */
#define  IS_RANGE	  2   /* Value out of range.		      */

    /* Floating point interrupt vectors used by math library	      */

#define  IS_1ST_FLOAT_VEC    0x34
#define  IS_LAST_FLOAT_VEC   0x3e
#define  IS_NUM_FLOAT_VECS  ((IS_LAST_FLOAT_VEC) - (IS_1ST_FLOAT_VEC) + 1)

    /* TURBO C TOOLS functions implemented as macros		      */

#define isgetvec(intype)  ((void far *) getvect(intype))

#define isputvec(intype,ptr) (setvect((intype),(void interrupt (*)())(ptr)))

#define isresext(excode)  (keep((excode),mmsize()))

    /* Function declarations					      */

int cdecl iscall(void far *,ALLREG *);/* Simulate software interrupt  */
				      /*			      */
				      /* Install a Turbo C TOOLS      */
int cdecl isinstal(int, 	      /* interrupt service routine    */
		   void (*)(ALLREG *,ISRCTRL *,ISRMSG *),
		   const char *,      /*			      */
		   ISRCTRL *,	      /*			      */
		   char *,	      /*			      */
		   int, 	      /*			      */
		   int);	      /*			      */
				      /*			      */
				      /* Prepare ISR control block    */
void cdecl isprep(void (*)(ALLREG *,ISRCTRL *,ISRMSG *),
		  const char *,       /*			      */
		  ISRCTRL *,	      /*			      */
		  char *,	      /*			      */
		  int,		      /*			      */
		  int); 	      /*			      */
				      /*			      */
ISRCTRL far * cdecl issense(	      /* Detect presence of Turbo C   */
	    void far *,const char *); /* TOOLS or C TOOLS PLUS or     */
				      /* LIGHT TOOLS interrupt	      */
				      /* service routine	      */
				      /*			      */
int cdecl isremove(unsigned);	      /* Remove resident program      */
				      /*			      */
unsigned cdecl iscurprc(int,unsigned);/* Return or set PSP of	      */
				      /* currently executing process. */
				      /*			      */
int cdecl isreserv(size_t,size_t,     /* Allocate block; also reserve */
		   char **);	      /* space for malloc() from ISR. */

#endif				      /* Ends "#ifndef DEF_BINTRUPT"  */
